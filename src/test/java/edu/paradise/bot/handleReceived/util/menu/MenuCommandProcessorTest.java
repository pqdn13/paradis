package edu.paradise.bot.handleReceived.util.menu;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MenuCommandProcessorTest {
    MenuCommandProcessor processor = MenuCommandProcessor.getInstanse();

    @Test
    void createCallBackCommand() {
        System.out.println(processor);
        String callBackCommand = processor.createCallBackCommand(MenuCommandType.BUY_MENU, 10);
        assertEquals(callBackCommand, "#" + MenuCommandType.BUY_MENU.toString()+ " " + MenuCommandArgs.ITEM_ID.toString()+"=10");
    }

    @Test
    void parseRawMenuCommand() {
    }
}