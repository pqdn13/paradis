package edu.paradise.bot.handleReceived.util.menu;

import java.util.Map;

public class MenuCommand {
    private MenuCommandType commandType;
    private Map<MenuCommandArgs, String> map;

    public MenuCommand(MenuCommandType commandType, Map<MenuCommandArgs, String> map) {
        this.commandType = commandType;
        this.map = map;
    }

    public MenuCommandType getCommandType() {
        return commandType;
    }

    public String getParamByType(MenuCommandArgs type){
        String s = map.get(type);
        if(s == null) throw new MenuCommandProcessorException("not find this argument: " + type.toString());
        return s;
    }

    @Override
    public String toString() {
        return "MenuCommand{" +
                "commandType=" + commandType +
                ", map=" + map +
                '}';
    }
}
