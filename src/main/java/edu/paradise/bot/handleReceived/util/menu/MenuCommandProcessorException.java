package edu.paradise.bot.handleReceived.util.menu;

public class MenuCommandProcessorException extends RuntimeException {
    public MenuCommandProcessorException() {
    }

    public MenuCommandProcessorException(String message) {
        super(message);
    }
}
