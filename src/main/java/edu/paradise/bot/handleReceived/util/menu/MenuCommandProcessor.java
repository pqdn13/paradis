package edu.paradise.bot.handleReceived.util.menu;

import java.util.HashMap;

public class MenuCommandProcessor {
    public static MenuCommandProcessor getInstanse(){
        return MenuCommandProcessor.Holder.HOLDER_INSTANCE;
    }

    public String createCallBackCommand(MenuCommandType command, Object... args) {
        String out = null;
        switch (command) {
            case BUY_MENU: case ITEM_MENU: case SHOP_MENU:
                if(args.length == 0 || !(args[0] instanceof Number)){
                    throw new MenuCommandProcessorException("bad args for command: " + command.toString());
                }

                out = new StringCommandBuilder(command)
                        .append(MenuCommandArgs.ITEM_ID, args[0])
                        .toString();
                break;
            default:
                throw new MenuCommandProcessorException("don't find handler for command: " + command.toString());
        }
        return out;
    }

    /**
     * требует оптимизации на поиск enum
     */
    public MenuCommand parseRawMenuCommand(String s) {
        if(s == null || s.length()<2 || !s.startsWith("#"))
            throw new MenuCommandProcessorException("invalid raw string command for parse: " + s);
        String[] split = s.split(" ");

        if(split[0].length() < 2)
            throw new MenuCommandProcessorException("invalid raw string command for parse: " + s);

        String command = split[0].substring(1).trim();
        MenuCommandType commandType = null;
        for (MenuCommandType type : MenuCommandType.values()) {
            if (command.equals(type.toString())) {
                commandType = type;
                break;
            }
        }

        if(commandType == null)
            throw new MenuCommandProcessorException("unknow command : " + command);

        HashMap<MenuCommandArgs, String> hashMap = new HashMap<>();
        for (int i = 1; i < split.length; i++) {
            String param = split[i];
            int indexOf = param.indexOf("=");
            String typeArg = null;
            String value = null;
            if (indexOf == -1) {
                typeArg = param;
            } else {
                if (indexOf == param.length() - 1) {
                    throw new MenuCommandProcessorException("invalid raw string command for parse (arg bad): " + s);
                }
                typeArg = param.substring(0, indexOf);
                value = param.substring(indexOf + 1);
            }

            MenuCommandArgs arg = null;
            for (MenuCommandArgs commandArg : MenuCommandArgs.values()) {
                if (typeArg.equals(commandArg.toString())) {
                    arg = commandArg;
                    break;
                }
            }

            if(arg == null)
                throw new MenuCommandProcessorException("unknow command (arg) : " + command);

            hashMap.put(arg, value);
        }

        return new MenuCommand(commandType, hashMap);
    }

    private class StringCommandBuilder{
        StringBuilder builder;
        StringCommandBuilder(MenuCommandType type) {
            this.builder = new StringBuilder()
                    .append('#')
                    .append(type.toString());
        }

        StringCommandBuilder append(MenuCommandArgs commandArg, Object value){
            if(value == null) throw new MenuCommandProcessorException("null arg : " + commandArg.toString() + " " + value);
            builder.append(' ')
                    .append(commandArg.toString())
                    .append('=')
                    .append(value.toString());
            return this;
        }

        StringCommandBuilder append(MenuCommandArgs commandArg){
            builder.append(' ')
                    .append(commandArg.toString());
            return this;
        }

        @Override
        public String toString() {
            return builder.toString();
        }
    }

    private MenuCommandProcessor() {
    }

    private static class Holder {
        static final MenuCommandProcessor HOLDER_INSTANCE = new MenuCommandProcessor();
    }
}
