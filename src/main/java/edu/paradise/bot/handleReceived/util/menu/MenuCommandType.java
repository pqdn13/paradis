package edu.paradise.bot.handleReceived.util.menu;

public enum MenuCommandType {
    ITEM_MENU("ItemMenu"),
    SHOP_MENU("ShopMenu"),
    BUY_MENU("BuyItem");


    private static final String prefix = "#";
    private String name;
    private String prefixname;

    MenuCommandType(String name) {
        this.name = name;
        this.prefixname = prefix + name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String toStringWithPrefix() {
        return prefixname;
    }
}
