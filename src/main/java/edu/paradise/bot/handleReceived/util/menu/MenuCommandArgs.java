package edu.paradise.bot.handleReceived.util.menu;

public enum MenuCommandArgs {
    ITEM_ID("itemId"),
    PAGE_NUMBER("pageNumber");

    private String name;

    MenuCommandArgs(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
