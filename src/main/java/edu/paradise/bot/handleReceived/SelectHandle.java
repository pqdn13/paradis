package edu.paradise.bot.handleReceived;

import edu.paradise.bot.handleReceived.example.Echo;
import edu.paradise.bot.handleReceived.classicMenu.Info;
import edu.paradise.bot.handleReceived.classicMenu.StartMenu;
import edu.paradise.bot.handleReceived.shopMenu.*;
import edu.paradise.bot.handleReceived.shopMenu.InitMenuPage;
import edu.paradise.bot.handleReceived.shopMenu.IncomeInvoice;
import edu.paradise.bot.handleReceived.shopMenu.ItemMenu;
import edu.paradise.bot.handleReceived.util.menu.MenuCommandProcessor;
import edu.paradise.bot.handleReceived.util.menu.MenuCommandType;
import edu.paradise.bot.strings.MenuStrings;
import org.telegram.telegrambots.api.objects.Update;

public class SelectHandle {
    private static MenuCommandProcessor menuCommandProcessor = MenuCommandProcessor.getInstanse();
    private static MenuStrings menuStrings = new MenuStrings();

    static public HandleUpdate getHandle(Update update) {
        if(update.hasMessage()) return getHandleMessage(update);
        if(update.hasCallbackQuery()) return getHandleCallbackQuery(update);
        return null;
    }

    private static HandleUpdate getHandleMessage(Update update){
        if(!update.getMessage().hasText()) return null;

        String text = update.getMessage().getText();
        if(text.equals("/start")) return new StartMenu(update);
        if(text.equals("/invoice")) return new IncomeInvoice(update);
        if(text.equals(menuStrings.getInfo())) return new Info(update);
        if(text.equals(menuStrings.getChooseProduct())) return new InitMenuPage(update);

        return new Echo(update);
    }

    private static HandleUpdate getHandleCallbackQuery(Update update){
        String data = update.getCallbackQuery().getData();
        if(data == null) return null;

        if(data.startsWith(MenuCommandType.ITEM_MENU.toStringWithPrefix())) return new ItemMenu(update);
        if(data.startsWith(MenuCommandType.SHOP_MENU.toStringWithPrefix())) return new ShopMenu(update);
        if(data.startsWith(MenuCommandType.BUY_MENU.toStringWithPrefix())) return new IncomeInvoice(update);

        return new Echo(update);
    }
}
