package edu.paradise.bot.handleReceived;

import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.api.objects.Update;

import java.io.Serializable;

public abstract class HandleUpdate<T extends Serializable, Method extends BotApiMethod<T>>{
    protected Update update;

    public HandleUpdate(Update update) {
        this.update = update;
    }

    public abstract Method generateAnswer();

    public Update getUpdate() {
        return update;
    }

    public void setUpdate(Update update) {
        this.update = update;
    }
}
