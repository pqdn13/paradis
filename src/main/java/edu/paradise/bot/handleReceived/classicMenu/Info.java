package edu.paradise.bot.handleReceived.classicMenu;

import edu.paradise.bot.handleReceived.HandleUpdate;
import edu.paradise.bot.strings.MenuStrings;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;

public class Info extends HandleUpdate<Message, SendMessage> {
    private MenuStrings menuStrings = new MenuStrings();

    public Info(Update update) {
        super(update);
    }

    @Override
    public SendMessage generateAnswer() {
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText(menuStrings.getInfoMessage())
                .enableHtml(true);
    }
}