package edu.paradise.bot.handleReceived.classicMenu;

import edu.paradise.bot.handleReceived.HandleUpdate;
import edu.paradise.bot.strings.MenuStrings;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

public class StartMenu extends HandleUpdate<Message, SendMessage> {
    private MenuStrings menuStrings = new MenuStrings();

    public StartMenu(Update update) {
        super(update);
    }

    @Override
    public SendMessage generateAnswer() {
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();

        row.add(menuStrings.getChooseProduct());
        row.add(menuStrings.getInfo());
        keyboard.add(row);

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup()
                .setResizeKeyboard(true)
                .setKeyboard(keyboard);

        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText(menuStrings.getInitMenuMessage())
                .setReplyMarkup(keyboardMarkup);
    }
}
