package edu.paradise.bot.handleReceived.example;

import edu.paradise.bot.handleReceived.HandleUpdate;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by m.paradise on 31.07.2017.
 */
public class CommonInlineKeyBoard extends HandleUpdate<Message, SendMessage> {
    public CommonInlineKeyBoard(Update update) {
        super(update);
    }

    @Override
    public SendMessage generateAnswer() {
        SendMessage message = null;
        if (update.hasMessage() && update.getMessage().hasText()) {
            List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();

            List<InlineKeyboardButton> row = new ArrayList<>();
            row.add(new InlineKeyboardButton().setText("btm11").setCallbackData("1"));
            row.add(new InlineKeyboardButton().setText("btm12").setCallbackData("2"));
            keyboard.add(row);

            row = new ArrayList<>();
            row.add(new InlineKeyboardButton().setText("btm21").setCallbackData("3"));
            row.add(new InlineKeyboardButton().setText("btm22").setCallbackData("4"));
            keyboard.add(row);

            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            inlineKeyboardMarkup.setKeyboard(keyboard);


            message = new SendMessage()
                    .setChatId(update.getMessage().getChatId().toString())
                    .setText("inline keyboard")
                    .disableNotification()
                    .setReplyMarkup(inlineKeyboardMarkup);
        }
        return message;
    }
}
