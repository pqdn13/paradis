package edu.paradise.bot.handleReceived.example;

import edu.paradise.bot.handleReceived.HandleUpdate;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EditInlineMessage extends HandleUpdate<Serializable, EditMessageText> {
    public EditInlineMessage(Update update) {
        super(update);
    }

    @Override
    public EditMessageText generateAnswer() {
        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();

        List<InlineKeyboardButton> row = new ArrayList<>();
        row.add(new InlineKeyboardButton().setText("btm11").setCallbackData("1"));
        row.add(new InlineKeyboardButton().setText("btm12").setCallbackData("2"));
        keyboard.add(row);

        row = new ArrayList<>();
        row.add(new InlineKeyboardButton().setText("btm21").setCallbackData("3"));
        row.add(new InlineKeyboardButton().setText("btm22").setCallbackData("4"));
        keyboard.add(row);

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(keyboard);


        CallbackQuery query = update.getCallbackQuery();
        EditMessageText editMessageText = new EditMessageText()
                .setChatId(query.getMessage().getChatId())
                .setText("new text" + query.getData())
                .setMessageId(query.getMessage().getMessageId())
                .setReplyMarkup(inlineKeyboardMarkup);

        return editMessageText;
    }
}
