package edu.paradise.bot.handleReceived.example;

import edu.paradise.bot.emoji.Emoji;
import edu.paradise.bot.handleReceived.HandleUpdate;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by m.paradise on 31.07.2017.
 */
public class CommonKeyBoard extends HandleUpdate<Message, SendMessage> {
    public CommonKeyBoard(Update update) {
        super(update);
    }

    @Override
    public SendMessage generateAnswer() {
        SendMessage message = null;
        if (update.hasMessage() && update.getMessage().hasText()) {
            message = new SendMessage();
            message.setChatId(update.getMessage().getChatId().toString());
            message.setText("init keyboard");

            // Create ReplyKeyboardMarkup object
            ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
            keyboardMarkup.setResizeKeyboard(true);

            // Create the keyboard (list of keyboard rows)
            List<KeyboardRow> keyboard = new ArrayList<>();
            // Create a keyboard row
            KeyboardRow row = new KeyboardRow();
            // Set each button, you can also use KeyboardButton objects if you need something else than text

            row.add(new KeyboardButton().setText(Emoji.CAT_FACE.toString()));

            row.add(Emoji.DOG_FACE.toString());
            row.add(Emoji.BEAR_FACE.toString());
            row.add(Emoji.FEARFUL_FACE.toString());


            // Add the first row to the keyboard
            keyboard.add(row);
            // Create another keyboard row
            row = new KeyboardRow();
            // Set each button for the second line
            row.add(Emoji.ANGRY_FACE.toString());
            row.add(Emoji.ASTONISHED_FACE.toString());
            row.add(Emoji.DRAGON_FACE.toString());
            row.add(Emoji.MONKEY_FACE.toString());
            // Add the second row to the keyboard
            keyboard.add(row);
            // Set the keyboard to the markup
            keyboardMarkup.setKeyboard(keyboard);

            // Add it to the message
            message.setReplyMarkup(keyboardMarkup);
        }

        return message;
    }
}
