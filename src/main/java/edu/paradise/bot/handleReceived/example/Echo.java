package edu.paradise.bot.handleReceived.example;

import edu.paradise.bot.emoji.Emoji;
import edu.paradise.bot.handleReceived.HandleUpdate;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;


public class Echo extends HandleUpdate<Message, SendMessage> {
    public Echo(Update update) {
        super(update);
    }

    @Override
    public SendMessage generateAnswer() {
        SendMessage message = null;
        if (update.hasMessage() && update.getMessage().hasText()) {
            message = new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText(
                            "<b>Sorry!</b><i> I don't know this command</i> " + Emoji.CONFOUNDED_FACE.toString()
                    )
                    .enableHtml(true)
                    .disableNotification()
                    .setReplyToMessageId(update.getMessage().getMessageId());
        }
        return message;
    }
}
