package edu.paradise.bot.handleReceived.shopMenu;

import edu.paradise.bot.handleReceived.HandleUpdate;
import edu.paradise.bot.utils.Filter;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.Update;


import java.io.Serializable;

import static edu.paradise.bot.utils.Filter.ELEMENTS_PER_PAGE;
import static edu.paradise.bot.utils.Filter.getPageNumberFromCallback;
import static edu.paradise.bot.utils.Filter.getSubMenu;


public class ShopMenu extends HandleUpdate<Serializable, EditMessageText> {

    public ShopMenu(Update update) {
        super(update);
    }

    @Override
    public EditMessageText generateAnswer() {
        String call_data = update.getCallbackQuery().getData();
        int message_id = update.getCallbackQuery().getMessage().getMessageId();
        long chat_id = update.getCallbackQuery().getMessage().getChatId();
        int pageNumber = getPageNumberFromCallback(call_data);
        int from = (pageNumber - 1) * ELEMENTS_PER_PAGE;
        EditMessageText editMessageText = new EditMessageText()
                .setChatId(chat_id)
                .setMessageId(message_id)
                .setText("Хочу это!")
                .setReplyMarkup(getSubMenu(from, from + 4, pageNumber));

        return editMessageText;
    }
}