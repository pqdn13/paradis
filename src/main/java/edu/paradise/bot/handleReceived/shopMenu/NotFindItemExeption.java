package edu.paradise.bot.handleReceived.shopMenu;

public class NotFindItemExeption extends RuntimeException {
    public NotFindItemExeption() {
    }

    public NotFindItemExeption(String message) {
        super(message);
    }
}
