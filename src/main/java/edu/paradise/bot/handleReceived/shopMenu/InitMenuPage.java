package edu.paradise.bot.handleReceived.shopMenu;

import edu.paradise.bot.handleReceived.HandleUpdate;
import edu.paradise.bot.utils.Filter;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;

import static edu.paradise.bot.utils.Filter.getSubMenu;

public class InitMenuPage extends HandleUpdate<Message, SendMessage> {

    public InitMenuPage(Update update) {
        super(update);
    }

    @Override
    public SendMessage generateAnswer() {
        SendMessage message = null;
        if (update.hasMessage() && update.getMessage().hasText()) {
            message = new SendMessage()
                    .setChatId(update.getMessage().getChatId().toString())
                    .setText("Хочу это!")
                    .setReplyMarkup(getSubMenu(0,4, 1));
        }
        return message;
    }
}
