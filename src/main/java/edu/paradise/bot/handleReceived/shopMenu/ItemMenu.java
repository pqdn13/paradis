package edu.paradise.bot.handleReceived.shopMenu;

import edu.paradise.bot.handleReceived.HandleUpdate;
import edu.paradise.bot.handleReceived.util.menu.MenuCommand;
import edu.paradise.bot.handleReceived.util.menu.MenuCommandArgs;
import edu.paradise.bot.handleReceived.util.menu.MenuCommandProcessor;
import edu.paradise.bot.handleReceived.util.menu.MenuCommandType;
import edu.paradise.bot.model.Item;
import edu.paradise.bot.service.ShopItemService;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static edu.paradise.bot.utils.Filter.getPageNumberFromCallback;

public class ItemMenu extends HandleUpdate<Serializable, EditMessageText> {
    private ShopItemService itemService = ShopItemService.getInstanse();
    private MenuCommandProcessor menuCommandProcessor = MenuCommandProcessor.getInstanse();

    public ItemMenu(Update update) {
        super(update);
    }

    @Override
    public EditMessageText generateAnswer() {

        String call_data = update.getCallbackQuery().getData();
        int message_id = update.getCallbackQuery().getMessage().getMessageId();
        long chat_id = update.getCallbackQuery().getMessage().getChatId();
        int pageNumber = getPageNumberFromCallback(call_data);
        Item item = getItemFromCallback(call_data);

        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();

        List<InlineKeyboardButton> row = new ArrayList<>();

        row.add(new InlineKeyboardButton()
                .setText("Купить")
                .setCallbackData(menuCommandProcessor.createCallBackCommand(MenuCommandType.BUY_MENU, item.getId()))
        );
        row.add(new InlineKeyboardButton()
                .setText("Назад")
                .setCallbackData(menuCommandProcessor.createCallBackCommand(MenuCommandType.SHOP_MENU, item.getId()) + " pageNumber=" + pageNumber)
        );
        keyboard.add(row);

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(keyboard);

        CallbackQuery query = update.getCallbackQuery();
        EditMessageText editMessageText = new EditMessageText()
                .setChatId(chat_id)
                .setText(
                        "[" + item.getName() +"](" + item.getUrlImg() + ")\n"
                                + item.getDescription() + "\n"
                                + "Стоимость: " + item.getPrice() / 100d + "\n"
                )
                .setMessageId(message_id)
                .setReplyMarkup(inlineKeyboardMarkup)
                .enableMarkdown(true);

        return editMessageText;
    }

    private Item getItemFromCallback(String data) {
        MenuCommand menuCommand = menuCommandProcessor.parseRawMenuCommand(data);
        long id = Long.parseLong(menuCommand.getParamByType(MenuCommandArgs.ITEM_ID));
        Item item = itemService.findItemById(id);

        if(item == null){
            throw new NotFindItemExeption("not find item by id=" + id);
        }

        return item;
    }
}