package edu.paradise.bot.handleReceived.shopMenu;

import edu.paradise.bot.handleReceived.HandleUpdate;
import edu.paradise.bot.handleReceived.util.menu.MenuCommandProcessor;
import edu.paradise.bot.handleReceived.util.menu.MenuCommandType;
import org.telegram.telegrambots.api.methods.send.SendInvoice;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.payments.LabeledPrice;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiValidationException;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.panasyuk on 03.08.2017.
 */
public class IncomeInvoice extends HandleUpdate<Message, SendInvoice> {

    private MenuCommandProcessor menuCommandProcessor = MenuCommandProcessor.getInstanse();

    public IncomeInvoice(Update update) {
        super(update);
    }

    @Override
    public SendInvoice generateAnswer() {

        List<LabeledPrice> price = new ArrayList<>();
        price.add(new LabeledPrice("33", 11100));
        
        SendInvoice inv = new SendInvoice()
                .setChatId(Integer.parseInt(update.getMessage().getChatId().toString()))
                .setDescription("The best running shoes 2017")
                .setPhotoUrl("https://2.bp.blogspot.com/-4EWBP3UEBDs/WTrJTwLVmMI/AAAAAAAAAk8/ZpbJ4c2WoocDpGtWL20n6TbcFYVq2pxZwCLcB/s320/1.png")
                .setPhotoWidth(90)
                .setPhotoHeight(50)
                .setTitle("Hello")
                .setPayload("telebot_test_invoice")
                .setProviderToken("361519591:TEST:d0322b287e92b987b6513f4565c8b0aa")
                .setStartParameter("StartParam")
                .setCurrency("RUB")
                .setPrices(price)
                .setNeedEmail(true);
        return inv;
    }
}
