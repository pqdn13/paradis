package edu.paradise.bot.service;

import edu.paradise.bot.model.Item;
import edu.paradise.bot.model.ShopInfo;

import java.util.ArrayList;
import java.util.List;

public class ShopItemService {
    private List<Item> items;
    private ShopInfo shopInfo;

    public static ShopItemService getInstanse(){
        return Holder.HOLDER_INSTANCE;
    }

    public List<Item> getItemList(){
        return items;
    }

    public List<Item> getItemSubList(int fromIndex, int toIndex){
        return items.subList(fromIndex, toIndex);
    }

    public Item findItemById(long id){
        for (Item item : items) {
            if(item.getId() == id) return item;
        }
        return null;
    }

    public Item findItemByName(String name){
        for (Item item : items) {
            if(item.getName().equals(name)) return item;
        }
        return null;
    }

    private ShopItemService() {
        items = new ArrayList<>();
        items.add(new Item(
                "Мексиканская",
                "Томатный соус, халапеньо, сладкий перец, цыпленок, томаты, шампиньоны, красный лук, моцарелла",
                1,35500));
        items.add(new Item(
                "Пепперони",
                "Томатный соус, пепперони, увеличенная порция моцареллы",
                2,35000));
        items.add(new Item(
                "Ранч пицца",
                "Соус Ранч, цыпленок, ветчина, томаты, чеснок, моцарелла",
                3,35000));
        items.add(new Item(
                "Сырный цыплёнок",
                "Сырный соус, цыпленок, томаты, моцарелла",
                4,34000));

        items.add(new Item(
                "Четыре сезона",
                "Томатный соус, пепперони, ветчина, брынза, томаты, шампиньоны, моцарелла, орегано",
                5,32000));
        items.add(new Item(
                "Чизбургер-пицца",
                "Сырный соус, говядина, бекон, соленые огурцы, томаты, красный лук, моцарелла",
                6,36500));
        items.add(new Item(
                "Цыплёнок барбекю",
                "Томатный соус, цыпленок, бекон, красный лук, моцарелла, соус Барбекю",
                7,35000));
        items.add(new Item(
                "Шаурма",
                "Томатный соус, цыпленок, бекон, капуста, огурцы и все что осталось",
                8,37000));
        items.add(new Item(
                "Шаурма1",
                "Томатный соус, пепперони, ветчина, брынза, томаты, шампиньоны, моцарелла, орегано",
                9,32000));
        items.add(new Item(
                "Шаурма2",
                "Сырный соус, говядина, бекон, соленые огурцы, томаты, красный лук, моцарелла",
                10,36500));
        items.add(new Item(
                "Шаурма3",
                "Томатный соус, цыпленок, бекон, красный лук, моцарелла, соус Барбекю",
                11,35000));
        items.add(new Item(
                "Шаурма4",
                "Томатный соус, цыпленок, бекон, капуста, огурцы и все что осталось",
                12,37000));

        shopInfo = new ShopInfo(
                "Пиццерия \"Вкусная сардиния\"",
                "Сильный продукт, передовые технологии и честные условия",
                "******"
        );
    }

    private static class Holder {
        static final ShopItemService HOLDER_INSTANCE = new ShopItemService();
    }
}
