package edu.paradise.bot.utils;

import edu.paradise.bot.handleReceived.shopMenu.NotFindItemExeption;
import edu.paradise.bot.handleReceived.util.menu.MenuCommand;
import edu.paradise.bot.handleReceived.util.menu.MenuCommandArgs;
import edu.paradise.bot.handleReceived.util.menu.MenuCommandProcessor;
import edu.paradise.bot.model.Item;
import edu.paradise.bot.service.ShopItemService;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.panasyuk on 03.08.2017.
 */
public class Filter {

    private static ShopItemService itemService = ShopItemService.getInstanse();

    private static MenuCommandProcessor menuCommandProcessor = MenuCommandProcessor.getInstanse();

    private List<Item> items = itemService.getItemList();

    /**
     * Member identifier for the current page number
     */
    private static int pageNumber = 1;

    /**
     * Member identifier for the current start page number in the page navigation
     */
    private int currentStartPageNo;

    /**
     * Member identifier for the current end page number in the page navigation
     */
    private int currentEndPageNo;

    /**
     * Member identifier for the number of elements on a page
     */
    public static final int ELEMENTS_PER_PAGE = 4;

    /**
     * Member identifier for the number of pages you have in the navigation (i.e 2 to  11 or 3 to 12 etc.)
     */
    private int pageNumberInNavigation;

    public static int getPageNumber() {
        return pageNumber;
    }

    public static void setPageNumber(int pageNumber) {
        Filter.pageNumber = pageNumber;
    }

    public static InlineKeyboardMarkup getSubMenu(int fromIndex, int toIndex, int pageNumber) {

        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        int indexTo = (pageNumber + 1) * ELEMENTS_PER_PAGE;
        if (pageNumber - 1 > 0 && (indexTo <= itemService.getItemList().size())) {
            row = fillMenuWithItems(fromIndex, toIndex, pageNumber, keyboard);
            row.add(new InlineKeyboardButton().setText("Назад").setCallbackData("#ShopMenu pageNumber=" + (pageNumber - 1)));
            row.add(new InlineKeyboardButton().setText("Вперед").setCallbackData("#ShopMenu pageNumber=" + (pageNumber + 1)));
        } else if (pageNumber - 1 <= 0 && (indexTo <= itemService.getItemList().size())) {
            row = fillMenuWithItems(fromIndex, toIndex, pageNumber, keyboard);
            row.add(new InlineKeyboardButton().setText("Вперед").setCallbackData("#ShopMenu pageNumber=" + (pageNumber + 1)));
        } else if (indexTo > itemService.getItemList().size()) {
            row = fillMenuWithItems(fromIndex, toIndex, pageNumber, keyboard);
            row.add(new InlineKeyboardButton().setText("Назад").setCallbackData("#ShopMenu pageNumber=" + (pageNumber - 1)));
        }
        keyboard.add(row);
        return new InlineKeyboardMarkup().setKeyboard(keyboard);
    }

    private static List<InlineKeyboardButton> fillMenuWithItems(int fromIndex, int toIndex, int pageNumber, List<List<InlineKeyboardButton>> keyboard) {
        List<Item> items = itemService.getItemSubList(fromIndex, toIndex);
        List<InlineKeyboardButton> row = new ArrayList<>();
        insertItem(row, items.get(0), pageNumber);
        insertItem(row, items.get(1), pageNumber);
        keyboard.add(row);

        row = new ArrayList<>();
        insertItem(row, items.get(2), pageNumber);
        insertItem(row, items.get(3), pageNumber);
        keyboard.add(row);

        row = new ArrayList<>();
        return row;
    }

    private static void insertItem(List<InlineKeyboardButton> row, Item item, long pageNumber) {
        row.add(new InlineKeyboardButton().setText(item.getName()).setCallbackData("#ItemMenu itemId=" + item.getId() + " pageNumber=" + pageNumber));
    }


    public static int getPageNumberFromCallback(String data) {
        MenuCommand menuCommand = menuCommandProcessor.parseRawMenuCommand(data);
        int pageNumber = Integer.parseInt(menuCommand.getParamByType(MenuCommandArgs.PAGE_NUMBER));
        if (pageNumber == 0 || pageNumber < 0) {
            throw new NotFindItemExeption("not find item by id=" + pageNumber);
        }
        return pageNumber;
    }

    public List<Item> createCountQuery() {
        return items;
    }

    ;

/*    public List<Item> createQuery(){

    };*/


    public int getAllElementsCount() {
        List<Item> list = createCountQuery();
        return !list.isEmpty() && list.get(0) != null ? list.size() : 0;
    }

    public List<Item> getElements() {
        int from = ((pageNumber - 1) * ELEMENTS_PER_PAGE);
        return items.subList(from, from + ELEMENTS_PER_PAGE);
    }

    public List getAllElements() {
        return items;
    }

    public void refresh() {
        //Your code here
    }

    public List<Item> next() {
        //Move to the next page if exists
        setPageNumber(getPageNumber() + 1);
        return getElements();
    }

    public List<Item> previous() {
        //Move to the previous page if exists
        setPageNumber(getPageNumber() - 1);
        return getElements();
    }

}
