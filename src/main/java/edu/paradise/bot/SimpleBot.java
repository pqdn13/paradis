package edu.paradise.bot;

import edu.paradise.bot.handleReceived.SelectHandle;
import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * name of bot - mypqdntestbot
 * nickname PqDnTestbot
 * token 414824939:AAGnPrnU8wFWMVoK2-eWpcLECy-Y37kQl48
 *
 * - Яндекс.Касса: тест: 381764678:TEST:1418 2017-08-01 08:19
 *
 * paradise t.me/PqDnTestbot
 *
 */
public class SimpleBot extends TelegramLongPollingBot {
    private String tokenBot;
    private String nicknameBot;

    public SimpleBot() {
        InputStream inputStream  = null;
        try {
            inputStream = getClass().getResourceAsStream("/bot-setting.properties.test");
            Properties properties = new Properties();
            properties.load(inputStream);
            tokenBot = properties.getProperty("tokenBot");
            nicknameBot = properties.getProperty("nicknameBot");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            BotApiMethod answer = SelectHandle.getHandle(update).generateAnswer();
            if (answer != null) {
                System.out.println(execute(answer).toString());
            }
        } catch (TelegramApiRequestException e) {
            // ok, no update msg
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotToken() {
        return tokenBot;
    }

    @Override
    public String getBotUsername() {
        return nicknameBot;
    }
}