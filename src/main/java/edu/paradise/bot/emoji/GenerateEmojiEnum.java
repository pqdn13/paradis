package edu.paradise.bot.emoji;

import java.io.*;
import java.util.Arrays;

public class GenerateEmojiEnum {
    public static void main(String[] args) throws Exception {


        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("emodji_row.txt")))) {
            FileWriter fileWriter = new FileWriter("emoji_code.txt");

            reader.lines().forEach((String line) -> {
                if(line.trim().length() < 8) return;

                String uCode = "0x" + line.substring(2, 7).trim();
                String discription = line.substring(7).trim();
                String nameEmoji = String.join("_", Arrays.asList(discription.toUpperCase().split(" ")));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < nameEmoji.length(); i++) {
                    builder.append(Character.isLetterOrDigit(nameEmoji.charAt(i)) ? nameEmoji.charAt(i) : '_');
                }

                nameEmoji = builder.toString();

                String o1 = "// " + line;
                String o2 = nameEmoji + "(" + uCode + "),";
                System.out.println(o1);
                System.out.println(o2);

                try {
                    fileWriter.write(o1 + '\n');
                    fileWriter.write(o2 + '\n' + '\n');
                } catch (IOException e) {
                    e.printStackTrace();
                }

            });

        }
    }
}
