package edu.paradise.bot.emoji;

/**
 * Emoji from https://apps.timwhitlock.info/emoji/tables/unicode
 * date is generate 28.07.2017
 *
 * @author m.paradise
 */
public enum Emoji {
    // U+1F601	grinning face with smiling eyes
    GRINNING_FACE_WITH_SMILING_EYES(0x1F601),

    // U+1F602	face with tears of joy
    FACE_WITH_TEARS_OF_JOY(0x1F602),

    // U+1F603	smiling face with open mouth
    SMILING_FACE_WITH_OPEN_MOUTH(0x1F603),

    // U+1F604	smiling face with open mouth and smiling eyes
    SMILING_FACE_WITH_OPEN_MOUTH_AND_SMILING_EYES(0x1F604),

    // U+1F605	smiling face with open mouth and cold sweat
    SMILING_FACE_WITH_OPEN_MOUTH_AND_COLD_SWEAT(0x1F605),

    // U+1F606	smiling face with open mouth and tightly-closed eyes
    SMILING_FACE_WITH_OPEN_MOUTH_AND_TIGHTLY_CLOSED_EYES(0x1F606),

    // U+1F609	winking face
    WINKING_FACE(0x1F609),

    // U+1F60A	smiling face with smiling eyes
    SMILING_FACE_WITH_SMILING_EYES(0x1F60A),

    // U+1F60B	face savouring delicious food
    FACE_SAVOURING_DELICIOUS_FOOD(0x1F60B),

    // U+1F60C	relieved face
    RELIEVED_FACE(0x1F60C),

    // U+1F60D	smiling face with heart-shaped eyes
    SMILING_FACE_WITH_HEART_SHAPED_EYES(0x1F60D),

    // U+1F60F	smirking face
    SMIRKING_FACE(0x1F60F),

    // U+1F612	unamused face
    UNAMUSED_FACE(0x1F612),

    // U+1F613	face with cold sweat
    FACE_WITH_COLD_SWEAT(0x1F613),

    // U+1F614	pensive face
    PENSIVE_FACE(0x1F614),

    // U+1F616	confounded face
    CONFOUNDED_FACE(0x1F616),

    // U+1F618	face throwing a kiss
    FACE_THROWING_A_KISS(0x1F618),

    // U+1F61A	kissing face with closed eyes
    KISSING_FACE_WITH_CLOSED_EYES(0x1F61A),

    // U+1F61C	face with stuck-out tongue and winking eye
    FACE_WITH_STUCK_OUT_TONGUE_AND_WINKING_EYE(0x1F61C),

    // U+1F61D	face with stuck-out tongue and tightly-closed eyes
    FACE_WITH_STUCK_OUT_TONGUE_AND_TIGHTLY_CLOSED_EYES(0x1F61D),

    // U+1F61E	disappointed face
    DISAPPOINTED_FACE(0x1F61E),

    // U+1F620	angry face
    ANGRY_FACE(0x1F620),

    // U+1F621	pouting face
    POUTING_FACE(0x1F621),

    // U+1F622	crying face
    CRYING_FACE(0x1F622),

    // U+1F623	persevering face
    PERSEVERING_FACE(0x1F623),

    // U+1F624	face with look of triumph
    FACE_WITH_LOOK_OF_TRIUMPH(0x1F624),

    // U+1F625	disappointed but relieved face
    DISAPPOINTED_BUT_RELIEVED_FACE(0x1F625),

    // U+1F628	fearful face
    FEARFUL_FACE(0x1F628),

    // U+1F629	weary face
    WEARY_FACE(0x1F629),

    // U+1F62A	sleepy face
    SLEEPY_FACE(0x1F62A),

    // U+1F62B	tired face
    TIRED_FACE(0x1F62B),

    // U+1F62D	loudly crying face
    LOUDLY_CRYING_FACE(0x1F62D),

    // U+1F630	face with open mouth and cold sweat
    FACE_WITH_OPEN_MOUTH_AND_COLD_SWEAT(0x1F630),

    // U+1F631	face screaming in fear
    FACE_SCREAMING_IN_FEAR(0x1F631),

    // U+1F632	astonished face
    ASTONISHED_FACE(0x1F632),

    // U+1F633	flushed face
    FLUSHED_FACE(0x1F633),

    // U+1F635	dizzy face
    DIZZY_FACE(0x1F635),

    // U+1F637	face with medical mask
    FACE_WITH_MEDICAL_MASK(0x1F637),

    // U+1F638	grinning cat face with smiling eyes
    GRINNING_CAT_FACE_WITH_SMILING_EYES(0x1F638),

    // U+1F639	cat face with tears of joy
    CAT_FACE_WITH_TEARS_OF_JOY(0x1F639),

    // U+1F63A	smiling cat face with open mouth
    SMILING_CAT_FACE_WITH_OPEN_MOUTH(0x1F63A),

    // U+1F63B	smiling cat face with heart-shaped eyes
    SMILING_CAT_FACE_WITH_HEART_SHAPED_EYES(0x1F63B),

    // U+1F63C	cat face with wry smile
    CAT_FACE_WITH_WRY_SMILE(0x1F63C),

    // U+1F63D	kissing cat face with closed eyes
    KISSING_CAT_FACE_WITH_CLOSED_EYES(0x1F63D),

    // U+1F63E	pouting cat face
    POUTING_CAT_FACE(0x1F63E),

    // U+1F63F	crying cat face
    CRYING_CAT_FACE(0x1F63F),

    // U+1F640	weary cat face
    WEARY_CAT_FACE(0x1F640),

    // U+1F645	face with no good gesture
    FACE_WITH_NO_GOOD_GESTURE(0x1F645),

    // U+1F646	face with ok gesture
    FACE_WITH_OK_GESTURE(0x1F646),

    // U+1F647	person bowing deeply
    PERSON_BOWING_DEEPLY(0x1F647),

    // U+1F648	see-no-evil monkey
    SEE_NO_EVIL_MONKEY(0x1F648),

    // U+1F649	hear-no-evil monkey
    HEAR_NO_EVIL_MONKEY(0x1F649),

    // U+1F64A	speak-no-evil monkey
    SPEAK_NO_EVIL_MONKEY(0x1F64A),

    // U+1F64B	happy person raising one hand
    HAPPY_PERSON_RAISING_ONE_HAND(0x1F64B),

    // U+1F64C	person raising both hands in celebration
    PERSON_RAISING_BOTH_HANDS_IN_CELEBRATION(0x1F64C),

    // U+1F64D	person frowning
    PERSON_FROWNING(0x1F64D),

    // U+1F64E	person with pouting face
    PERSON_WITH_POUTING_FACE(0x1F64E),

    // U+1F64F	person with folded hands
    PERSON_WITH_FOLDED_HANDS(0x1F64F),

    // U+2702	black scissors
    BLACK_SCISSORS(0x2702),

    // U+2705	white heavy check mark
    WHITE_HEAVY_CHECK_MARK(0x2705),

    // U+2708	airplane
    AIRPLANE(0x2708),

    // U+2709	envelope
    ENVELOPE(0x2709),

    // U+270A	raised fist
    RAISED_FIST(0x270A),

    // U+270B	raised hand
    RAISED_HAND(0x270B),

    // U+270C	victory hand
    VICTORY_HAND(0x270C),

    // U+270F	pencil
    PENCIL(0x270F),

    // U+2712	black nib
    BLACK_NIB(0x2712),

    // U+2714	heavy check mark
    HEAVY_CHECK_MARK(0x2714),

    // U+2716	heavy multiplication x
    HEAVY_MULTIPLICATION_X(0x2716),

    // U+2728	sparkles
    SPARKLES(0x2728),

    // U+2733	eight spoked asterisk
    EIGHT_SPOKED_ASTERISK(0x2733),

    // U+2734	eight pointed black star
    EIGHT_POINTED_BLACK_STAR(0x2734),

    // U+2744	snowflake
    SNOWFLAKE(0x2744),

    // U+2747	sparkle
    SPARKLE(0x2747),

    // U+274C	cross mark
    CROSS_MARK(0x274C),

    // U+274E	negative squared cross mark
    NEGATIVE_SQUARED_CROSS_MARK(0x274E),

    // U+2753	black question mark ornament
    BLACK_QUESTION_MARK_ORNAMENT(0x2753),

    // U+2754	white question mark ornament
    WHITE_QUESTION_MARK_ORNAMENT(0x2754),

    // U+2755	white exclamation mark ornament
    WHITE_EXCLAMATION_MARK_ORNAMENT(0x2755),

    // U+2757	heavy exclamation mark symbol
    HEAVY_EXCLAMATION_MARK_SYMBOL(0x2757),

    // U+2764	heavy black heart
    HEAVY_BLACK_HEART(0x2764),

    // U+2795	heavy plus sign
    HEAVY_PLUS_SIGN(0x2795),

    // U+2796	heavy minus sign
    HEAVY_MINUS_SIGN(0x2796),

    // U+2797	heavy division sign
    HEAVY_DIVISION_SIGN(0x2797),

    // U+27A1	black rightwards arrow
    BLACK_RIGHTWARDS_ARROW(0x27A1),

    // U+27B0	curly loop
    CURLY_LOOP(0x27B0),

    // U+1F680	rocket
    ROCKET(0x1F680),

    // U+1F683	railway car
    RAILWAY_CAR(0x1F683),

    // U+1F684	high-speed train
    HIGH_SPEED_TRAIN(0x1F684),

    // U+1F685	high-speed train with bullet nose
    HIGH_SPEED_TRAIN_WITH_BULLET_NOSE(0x1F685),

    // U+1F687	metro
    METRO(0x1F687),

    // U+1F689	station
    STATION(0x1F689),

    // U+1F68C	bus
    BUS(0x1F68C),

    // U+1F68F	bus stop
    BUS_STOP(0x1F68F),

    // U+1F691	ambulance
    AMBULANCE(0x1F691),

    // U+1F692	fire engine
    FIRE_ENGINE(0x1F692),

    // U+1F693	police car
    POLICE_CAR(0x1F693),

    // U+1F695	taxi
    TAXI(0x1F695),

    // U+1F697	automobile
    AUTOMOBILE(0x1F697),

    // U+1F699	recreational vehicle
    RECREATIONAL_VEHICLE(0x1F699),

    // U+1F69A	delivery truck
    DELIVERY_TRUCK(0x1F69A),

    // U+1F6A2	ship
    SHIP(0x1F6A2),

    // U+1F6A4	speedboat
    SPEEDBOAT(0x1F6A4),

    // U+1F6A5	horizontal traffic light
    HORIZONTAL_TRAFFIC_LIGHT(0x1F6A5),

    // U+1F6A7	construction sign
    CONSTRUCTION_SIGN(0x1F6A7),

    // U+1F6A8	police cars revolving light
    POLICE_CARS_REVOLVING_LIGHT(0x1F6A8),

    // U+1F6A9	triangular flag on post
    TRIANGULAR_FLAG_ON_POST(0x1F6A9),

    // U+1F6AA	door
    DOOR(0x1F6AA),

    // U+1F6AB	no entry sign
    NO_ENTRY_SIGN(0x1F6AB),

    // U+1F6AC	smoking symbol
    SMOKING_SYMBOL(0x1F6AC),

    // U+1F6AD	no smoking symbol
    NO_SMOKING_SYMBOL(0x1F6AD),

    // U+1F6B2	bicycle
    BICYCLE(0x1F6B2),

    // U+1F6B6	pedestrian
    PEDESTRIAN(0x1F6B6),

    // U+1F6B9	mens symbol
    MENS_SYMBOL(0x1F6B9),

    // U+1F6BA	womens symbol
    WOMENS_SYMBOL(0x1F6BA),

    // U+1F6BB	restroom
    RESTROOM(0x1F6BB),

    // U+1F6BC	baby symbol
    BABY_SYMBOL(0x1F6BC),

    // U+1F6BD	toilet
    TOILET(0x1F6BD),

    // U+1F6BE	water closet
    WATER_CLOSET(0x1F6BE),

    // U+1F6C0	bath
    BATH(0x1F6C0),

    // U+24C2	circled latin capital letter m
    CIRCLED_LATIN_CAPITAL_LETTER_M(0x24C2),

    // U+1F170	negative squared latin capital letter a
    NEGATIVE_SQUARED_LATIN_CAPITAL_LETTER_A(0x1F170),

    // U+1F171	negative squared latin capital letter b
    NEGATIVE_SQUARED_LATIN_CAPITAL_LETTER_B(0x1F171),

    // U+1F17E	negative squared latin capital letter o
    NEGATIVE_SQUARED_LATIN_CAPITAL_LETTER_O(0x1F17E),

    // U+1F17F	negative squared latin capital letter p
    NEGATIVE_SQUARED_LATIN_CAPITAL_LETTER_P(0x1F17F),

    // U+1F18E	negative squared ab
    NEGATIVE_SQUARED_AB(0x1F18E),

    // U+1F191	squared cl
    SQUARED_CL(0x1F191),

    // U+1F192	squared cool
    SQUARED_COOL(0x1F192),

    // U+1F193	squared free
    SQUARED_FREE(0x1F193),

    // U+1F194	squared id
    SQUARED_ID(0x1F194),

    // U+1F195	squared new
    SQUARED_NEW(0x1F195),

    // U+1F196	squared ng
    SQUARED_NG(0x1F196),

    // U+1F197	squared ok
    SQUARED_OK(0x1F197),

    // U+1F198	squared sos
    SQUARED_SOS(0x1F198),

    // U+1F199	squared up with exclamation mark
    SQUARED_UP_WITH_EXCLAMATION_MARK(0x1F199),

    // U+1F19A	squared vs
    SQUARED_VS(0x1F19A),

    // U+1F1E9 U+1F1EA	regional indicator symbol letter d + regional indicator symbol letter e
    U_1F1EA_REGIONAL_INDICATOR_SYMBOL_LETTER_D___REGIONAL_INDICATOR_SYMBOL_LETTER_E(0x1F1E9),

    // U+1F1EC U+1F1E7	regional indicator symbol letter g + regional indicator symbol letter b
    U_1F1E7_REGIONAL_INDICATOR_SYMBOL_LETTER_G___REGIONAL_INDICATOR_SYMBOL_LETTER_B(0x1F1EC),

    // U+1F1E8 U+1F1F3	regional indicator symbol letter c + regional indicator symbol letter n
    U_1F1F3_REGIONAL_INDICATOR_SYMBOL_LETTER_C___REGIONAL_INDICATOR_SYMBOL_LETTER_N(0x1F1E8),

    // U+1F1EF U+1F1F5	regional indicator symbol letter j + regional indicator symbol letter p
    U_1F1F5_REGIONAL_INDICATOR_SYMBOL_LETTER_J___REGIONAL_INDICATOR_SYMBOL_LETTER_P(0x1F1EF),

    // U+1F1F0 U+1F1F7	regional indicator symbol letter k + regional indicator symbol letter r
    U_1F1F7_REGIONAL_INDICATOR_SYMBOL_LETTER_K___REGIONAL_INDICATOR_SYMBOL_LETTER_R(0x1F1F0),

    // U+1F1EB U+1F1F7	regional indicator symbol letter f + regional indicator symbol letter r
    U_1F1F7_REGIONAL_INDICATOR_SYMBOL_LETTER_F___REGIONAL_INDICATOR_SYMBOL_LETTER_R(0x1F1EB),

    // U+1F1EA U+1F1F8	regional indicator symbol letter e + regional indicator symbol letter s
    U_1F1F8_REGIONAL_INDICATOR_SYMBOL_LETTER_E___REGIONAL_INDICATOR_SYMBOL_LETTER_S(0x1F1EA),

    // U+1F1EE U+1F1F9	regional indicator symbol letter i + regional indicator symbol letter t
    U_1F1F9_REGIONAL_INDICATOR_SYMBOL_LETTER_I___REGIONAL_INDICATOR_SYMBOL_LETTER_T(0x1F1EE),

    // U+1F1FA U+1F1F8	regional indicator symbol letter u + regional indicator symbol letter s
    U_1F1F8_REGIONAL_INDICATOR_SYMBOL_LETTER_U___REGIONAL_INDICATOR_SYMBOL_LETTER_S(0x1F1FA),

    // U+1F1F7 U+1F1FA	regional indicator symbol letter r + regional indicator symbol letter u
    U_1F1FA_REGIONAL_INDICATOR_SYMBOL_LETTER_R___REGIONAL_INDICATOR_SYMBOL_LETTER_U(0x1F1F7),

    // U+1F201	squared katakana koko
    SQUARED_KATAKANA_KOKO(0x1F201),

    // U+1F202	squared katakana sa
    SQUARED_KATAKANA_SA(0x1F202),

    // U+1F21A	squared cjk unified ideograph-7121
    SQUARED_CJK_UNIFIED_IDEOGRAPH_7121(0x1F21A),

    // U+1F22F	squared cjk unified ideograph-6307
    SQUARED_CJK_UNIFIED_IDEOGRAPH_6307(0x1F22F),

    // U+1F232	squared cjk unified ideograph-7981
    SQUARED_CJK_UNIFIED_IDEOGRAPH_7981(0x1F232),

    // U+1F233	squared cjk unified ideograph-7a7a
    SQUARED_CJK_UNIFIED_IDEOGRAPH_7A7A(0x1F233),

    // U+1F234	squared cjk unified ideograph-5408
    SQUARED_CJK_UNIFIED_IDEOGRAPH_5408(0x1F234),

    // U+1F235	squared cjk unified ideograph-6e80
    SQUARED_CJK_UNIFIED_IDEOGRAPH_6E80(0x1F235),

    // U+1F236	squared cjk unified ideograph-6709
    SQUARED_CJK_UNIFIED_IDEOGRAPH_6709(0x1F236),

    // U+1F237	squared cjk unified ideograph-6708
    SQUARED_CJK_UNIFIED_IDEOGRAPH_6708(0x1F237),

    // U+1F238	squared cjk unified ideograph-7533
    SQUARED_CJK_UNIFIED_IDEOGRAPH_7533(0x1F238),

    // U+1F239	squared cjk unified ideograph-5272
    SQUARED_CJK_UNIFIED_IDEOGRAPH_5272(0x1F239),

    // U+1F23A	squared cjk unified ideograph-55b6
    SQUARED_CJK_UNIFIED_IDEOGRAPH_55B6(0x1F23A),

    // U+1F250	circled ideograph advantage
    CIRCLED_IDEOGRAPH_ADVANTAGE(0x1F250),

    // U+1F251	circled ideograph accept
    CIRCLED_IDEOGRAPH_ACCEPT(0x1F251),

    // U+00A9	copyright sign
    COPYRIGHT_SIGN(0x00A9),

    // U+00AE	registered sign
    REGISTERED_SIGN(0x00AE),

    // U+203C	double exclamation mark
    DOUBLE_EXCLAMATION_MARK(0x203C),

    // U+2049	exclamation question mark
    EXCLAMATION_QUESTION_MARK(0x2049),

    // U+0038 U+20E3	digit eight + combining enclosing keycap
    U_20E3_DIGIT_EIGHT___COMBINING_ENCLOSING_KEYCAP(0x0038),

    // U+0039 U+20E3	digit nine + combining enclosing keycap
    U_20E3_DIGIT_NINE___COMBINING_ENCLOSING_KEYCAP(0x0039),

    // U+0037 U+20E3	digit seven + combining enclosing keycap
    U_20E3_DIGIT_SEVEN___COMBINING_ENCLOSING_KEYCAP(0x0037),

    // U+0036 U+20E3	digit six + combining enclosing keycap
    U_20E3_DIGIT_SIX___COMBINING_ENCLOSING_KEYCAP(0x0036),

    // U+0031 U+20E3	digit one + combining enclosing keycap
    U_20E3_DIGIT_ONE___COMBINING_ENCLOSING_KEYCAP(0x0031),

    // U+0030 U+20E3	digit zero + combining enclosing keycap
    U_20E3_DIGIT_ZERO___COMBINING_ENCLOSING_KEYCAP(0x0030),

    // U+0032 U+20E3	digit two + combining enclosing keycap
    U_20E3_DIGIT_TWO___COMBINING_ENCLOSING_KEYCAP(0x0032),

    // U+0033 U+20E3	digit three + combining enclosing keycap
    U_20E3_DIGIT_THREE___COMBINING_ENCLOSING_KEYCAP(0x0033),

    // U+0035 U+20E3	digit five + combining enclosing keycap
    U_20E3_DIGIT_FIVE___COMBINING_ENCLOSING_KEYCAP(0x0035),

    // U+0034 U+20E3	digit four + combining enclosing keycap
    U_20E3_DIGIT_FOUR___COMBINING_ENCLOSING_KEYCAP(0x0034),

    // U+0023 U+20E3	number sign + combining enclosing keycap
    U_20E3_NUMBER_SIGN___COMBINING_ENCLOSING_KEYCAP(0x0023),

    // U+2122	trade mark sign
    TRADE_MARK_SIGN(0x2122),

    // U+2139	information source
    INFORMATION_SOURCE(0x2139),

    // U+2194	left right arrow
    LEFT_RIGHT_ARROW(0x2194),

    // U+2195	up down arrow
    UP_DOWN_ARROW(0x2195),

    // U+2196	north west arrow
    NORTH_WEST_ARROW(0x2196),

    // U+2197	north east arrow
    NORTH_EAST_ARROW(0x2197),

    // U+2198	south east arrow
    SOUTH_EAST_ARROW(0x2198),

    // U+2199	south west arrow
    SOUTH_WEST_ARROW(0x2199),

    // U+21A9	leftwards arrow with hook
    LEFTWARDS_ARROW_WITH_HOOK(0x21A9),

    // U+21AA	rightwards arrow with hook
    RIGHTWARDS_ARROW_WITH_HOOK(0x21AA),

    // U+231A	watch
    WATCH(0x231A),

    // U+231B	hourglass
    HOURGLASS(0x231B),

    // U+23E9	black right-pointing double triangle
    BLACK_RIGHT_POINTING_DOUBLE_TRIANGLE(0x23E9),

    // U+23EA	black left-pointing double triangle
    BLACK_LEFT_POINTING_DOUBLE_TRIANGLE(0x23EA),

    // U+23EB	black up-pointing double triangle
    BLACK_UP_POINTING_DOUBLE_TRIANGLE(0x23EB),

    // U+23EC	black down-pointing double triangle
    BLACK_DOWN_POINTING_DOUBLE_TRIANGLE(0x23EC),

    // U+23F0	alarm clock
    ALARM_CLOCK(0x23F0),

    // U+23F3	hourglass with flowing sand
    HOURGLASS_WITH_FLOWING_SAND(0x23F3),

    // U+25AA	black small square
    BLACK_SMALL_SQUARE(0x25AA),

    // U+25AB	white small square
    WHITE_SMALL_SQUARE(0x25AB),

    // U+25B6	black right-pointing triangle
    BLACK_RIGHT_POINTING_TRIANGLE(0x25B6),

    // U+25C0	black left-pointing triangle
    BLACK_LEFT_POINTING_TRIANGLE(0x25C0),

    // U+25FB	white medium square
    WHITE_MEDIUM_SQUARE(0x25FB),

    // U+25FC	black medium square
    BLACK_MEDIUM_SQUARE(0x25FC),

    // U+25FD	white medium small square
    WHITE_MEDIUM_SMALL_SQUARE(0x25FD),

    // U+25FE	black medium small square
    BLACK_MEDIUM_SMALL_SQUARE(0x25FE),

    // U+2600	black sun with rays
    BLACK_SUN_WITH_RAYS(0x2600),

    // U+2601	cloud
    CLOUD(0x2601),

    // U+260E	black telephone
    BLACK_TELEPHONE(0x260E),

    // U+2611	ballot box with check
    BALLOT_BOX_WITH_CHECK(0x2611),

    // U+2614	umbrella with rain drops
    UMBRELLA_WITH_RAIN_DROPS(0x2614),

    // U+2615	hot beverage
    HOT_BEVERAGE(0x2615),

    // U+261D	white up pointing index
    WHITE_UP_POINTING_INDEX(0x261D),

    // U+263A	white smiling face
    WHITE_SMILING_FACE(0x263A),

    // U+2648	aries
    ARIES(0x2648),

    // U+2649	taurus
    TAURUS(0x2649),

    // U+264A	gemini
    GEMINI(0x264A),

    // U+264B	cancer
    CANCER(0x264B),

    // U+264C	leo
    LEO(0x264C),

    // U+264D	virgo
    VIRGO(0x264D),

    // U+264E	libra
    LIBRA(0x264E),

    // U+264F	scorpius
    SCORPIUS(0x264F),

    // U+2650	sagittarius
    SAGITTARIUS(0x2650),

    // U+2651	capricorn
    CAPRICORN(0x2651),

    // U+2652	aquarius
    AQUARIUS(0x2652),

    // U+2653	pisces
    PISCES(0x2653),

    // U+2660	black spade suit
    BLACK_SPADE_SUIT(0x2660),

    // U+2663	black club suit
    BLACK_CLUB_SUIT(0x2663),

    // U+2665	black heart suit
    BLACK_HEART_SUIT(0x2665),

    // U+2666	black diamond suit
    BLACK_DIAMOND_SUIT(0x2666),

    // U+2668	hot springs
    HOT_SPRINGS(0x2668),

    // U+267B	black universal recycling symbol
    BLACK_UNIVERSAL_RECYCLING_SYMBOL(0x267B),

    // U+267F	wheelchair symbol
    WHEELCHAIR_SYMBOL(0x267F),

    // U+2693	anchor
    ANCHOR(0x2693),

    // U+26A0	warning sign
    WARNING_SIGN(0x26A0),

    // U+26A1	high voltage sign
    HIGH_VOLTAGE_SIGN(0x26A1),

    // U+26AA	medium white circle
    MEDIUM_WHITE_CIRCLE(0x26AA),

    // U+26AB	medium black circle
    MEDIUM_BLACK_CIRCLE(0x26AB),

    // U+26BD	soccer ball
    SOCCER_BALL(0x26BD),

    // U+26BE	baseball
    BASEBALL(0x26BE),

    // U+26C4	snowman without snow
    SNOWMAN_WITHOUT_SNOW(0x26C4),

    // U+26C5	sun behind cloud
    SUN_BEHIND_CLOUD(0x26C5),

    // U+26CE	ophiuchus
    OPHIUCHUS(0x26CE),

    // U+26D4	no entry
    NO_ENTRY(0x26D4),

    // U+26EA	church
    CHURCH(0x26EA),

    // U+26F2	fountain
    FOUNTAIN(0x26F2),

    // U+26F3	flag in hole
    FLAG_IN_HOLE(0x26F3),

    // U+26F5	sailboat
    SAILBOAT(0x26F5),

    // U+26FA	tent
    TENT(0x26FA),

    // U+26FD	fuel pump
    FUEL_PUMP(0x26FD),

    // U+2934	arrow pointing rightwards then curving upwards
    ARROW_POINTING_RIGHTWARDS_THEN_CURVING_UPWARDS(0x2934),

    // U+2935	arrow pointing rightwards then curving downwards
    ARROW_POINTING_RIGHTWARDS_THEN_CURVING_DOWNWARDS(0x2935),

    // U+2B05	leftwards black arrow
    LEFTWARDS_BLACK_ARROW(0x2B05),

    // U+2B06	upwards black arrow
    UPWARDS_BLACK_ARROW(0x2B06),

    // U+2B07	downwards black arrow
    DOWNWARDS_BLACK_ARROW(0x2B07),

    // U+2B1B	black large square
    BLACK_LARGE_SQUARE(0x2B1B),

    // U+2B1C	white large square
    WHITE_LARGE_SQUARE(0x2B1C),

    // U+2B50	white medium star
    WHITE_MEDIUM_STAR(0x2B50),

    // U+2B55	heavy large circle
    HEAVY_LARGE_CIRCLE(0x2B55),

    // U+3030	wavy dash
    WAVY_DASH(0x3030),

    // U+303D	part alternation mark
    PART_ALTERNATION_MARK(0x303D),

    // U+3297	circled ideograph congratulation
    CIRCLED_IDEOGRAPH_CONGRATULATION(0x3297),

    // U+3299	circled ideograph secret
    CIRCLED_IDEOGRAPH_SECRET(0x3299),

    // U+1F004	mahjong tile red dragon
    MAHJONG_TILE_RED_DRAGON(0x1F004),

    // U+1F0CF	playing card black joker
    PLAYING_CARD_BLACK_JOKER(0x1F0CF),

    // U+1F300	cyclone
    CYCLONE(0x1F300),

    // U+1F301	foggy
    FOGGY(0x1F301),

    // U+1F302	closed umbrella
    CLOSED_UMBRELLA(0x1F302),

    // U+1F303	night with stars
    NIGHT_WITH_STARS(0x1F303),

    // U+1F304	sunrise over mountains
    SUNRISE_OVER_MOUNTAINS(0x1F304),

    // U+1F305	sunrise
    SUNRISE(0x1F305),

    // U+1F306	cityscape at dusk
    CITYSCAPE_AT_DUSK(0x1F306),

    // U+1F307	sunset over buildings
    SUNSET_OVER_BUILDINGS(0x1F307),

    // U+1F308	rainbow
    RAINBOW(0x1F308),

    // U+1F309	bridge at night
    BRIDGE_AT_NIGHT(0x1F309),

    // U+1F30A	water wave
    WATER_WAVE(0x1F30A),

    // U+1F30B	volcano
    VOLCANO(0x1F30B),

    // U+1F30C	milky way
    MILKY_WAY(0x1F30C),

    // U+1F30F	earth globe asia-australia
    EARTH_GLOBE_ASIA_AUSTRALIA(0x1F30F),

    // U+1F311	new moon symbol
    NEW_MOON_SYMBOL(0x1F311),

    // U+1F313	first quarter moon symbol
    FIRST_QUARTER_MOON_SYMBOL(0x1F313),

    // U+1F314	waxing gibbous moon symbol
    WAXING_GIBBOUS_MOON_SYMBOL(0x1F314),

    // U+1F315	full moon symbol
    FULL_MOON_SYMBOL(0x1F315),

    // U+1F319	crescent moon
    CRESCENT_MOON(0x1F319),

    // U+1F31B	first quarter moon with face
    FIRST_QUARTER_MOON_WITH_FACE(0x1F31B),

    // U+1F31F	glowing star
    GLOWING_STAR(0x1F31F),

    // U+1F320	shooting star
    SHOOTING_STAR(0x1F320),

    // U+1F330	chestnut
    CHESTNUT(0x1F330),

    // U+1F331	seedling
    SEEDLING(0x1F331),

    // U+1F334	palm tree
    PALM_TREE(0x1F334),

    // U+1F335	cactus
    CACTUS(0x1F335),

    // U+1F337	tulip
    TULIP(0x1F337),

    // U+1F338	cherry blossom
    CHERRY_BLOSSOM(0x1F338),

    // U+1F339	rose
    ROSE(0x1F339),

    // U+1F33A	hibiscus
    HIBISCUS(0x1F33A),

    // U+1F33B	sunflower
    SUNFLOWER(0x1F33B),

    // U+1F33C	blossom
    BLOSSOM(0x1F33C),

    // U+1F33D	ear of maize
    EAR_OF_MAIZE(0x1F33D),

    // U+1F33E	ear of rice
    EAR_OF_RICE(0x1F33E),

    // U+1F33F	herb
    HERB(0x1F33F),

    // U+1F340	four leaf clover
    FOUR_LEAF_CLOVER(0x1F340),

    // U+1F341	maple leaf
    MAPLE_LEAF(0x1F341),

    // U+1F342	fallen leaf
    FALLEN_LEAF(0x1F342),

    // U+1F343	leaf fluttering in wind
    LEAF_FLUTTERING_IN_WIND(0x1F343),

    // U+1F344	mushroom
    MUSHROOM(0x1F344),

    // U+1F345	tomato
    TOMATO(0x1F345),

    // U+1F346	aubergine
    AUBERGINE(0x1F346),

    // U+1F347	grapes
    GRAPES(0x1F347),

    // U+1F348	melon
    MELON(0x1F348),

    // U+1F349	watermelon
    WATERMELON(0x1F349),

    // U+1F34A	tangerine
    TANGERINE(0x1F34A),

    // U+1F34C	banana
    BANANA(0x1F34C),

    // U+1F34D	pineapple
    PINEAPPLE(0x1F34D),

    // U+1F34E	red apple
    RED_APPLE(0x1F34E),

    // U+1F34F	green apple
    GREEN_APPLE(0x1F34F),

    // U+1F351	peach
    PEACH(0x1F351),

    // U+1F352	cherries
    CHERRIES(0x1F352),

    // U+1F353	strawberry
    STRAWBERRY(0x1F353),

    // U+1F354	hamburger
    HAMBURGER(0x1F354),

    // U+1F355	slice of pizza
    SLICE_OF_PIZZA(0x1F355),

    // U+1F356	meat on bone
    MEAT_ON_BONE(0x1F356),

    // U+1F357	poultry leg
    POULTRY_LEG(0x1F357),

    // U+1F358	rice cracker
    RICE_CRACKER(0x1F358),

    // U+1F359	rice ball
    RICE_BALL(0x1F359),

    // U+1F35A	cooked rice
    COOKED_RICE(0x1F35A),

    // U+1F35B	curry and rice
    CURRY_AND_RICE(0x1F35B),

    // U+1F35C	steaming bowl
    STEAMING_BOWL(0x1F35C),

    // U+1F35D	spaghetti
    SPAGHETTI(0x1F35D),

    // U+1F35E	bread
    BREAD(0x1F35E),

    // U+1F35F	french fries
    FRENCH_FRIES(0x1F35F),

    // U+1F360	roasted sweet potato
    ROASTED_SWEET_POTATO(0x1F360),

    // U+1F361	dango
    DANGO(0x1F361),

    // U+1F362	oden
    ODEN(0x1F362),

    // U+1F363	sushi
    SUSHI(0x1F363),

    // U+1F364	fried shrimp
    FRIED_SHRIMP(0x1F364),

    // U+1F365	fish cake with swirl design
    FISH_CAKE_WITH_SWIRL_DESIGN(0x1F365),

    // U+1F366	soft ice cream
    SOFT_ICE_CREAM(0x1F366),

    // U+1F367	shaved ice
    SHAVED_ICE(0x1F367),

    // U+1F368	ice cream
    ICE_CREAM(0x1F368),

    // U+1F369	doughnut
    DOUGHNUT(0x1F369),

    // U+1F36A	cookie
    COOKIE(0x1F36A),

    // U+1F36B	chocolate bar
    CHOCOLATE_BAR(0x1F36B),

    // U+1F36C	candy
    CANDY(0x1F36C),

    // U+1F36D	lollipop
    LOLLIPOP(0x1F36D),

    // U+1F36E	custard
    CUSTARD(0x1F36E),

    // U+1F36F	honey pot
    HONEY_POT(0x1F36F),

    // U+1F370	shortcake
    SHORTCAKE(0x1F370),

    // U+1F371	bento box
    BENTO_BOX(0x1F371),

    // U+1F372	pot of food
    POT_OF_FOOD(0x1F372),

    // U+1F373	cooking
    COOKING(0x1F373),

    // U+1F374	fork and knife
    FORK_AND_KNIFE(0x1F374),

    // U+1F375	teacup without handle
    TEACUP_WITHOUT_HANDLE(0x1F375),

    // U+1F376	sake bottle and cup
    SAKE_BOTTLE_AND_CUP(0x1F376),

    // U+1F377	wine glass
    WINE_GLASS(0x1F377),

    // U+1F378	cocktail glass
    COCKTAIL_GLASS(0x1F378),

    // U+1F379	tropical drink
    TROPICAL_DRINK(0x1F379),

    // U+1F37A	beer mug
    BEER_MUG(0x1F37A),

    // U+1F37B	clinking beer mugs
    CLINKING_BEER_MUGS(0x1F37B),

    // U+1F380	ribbon
    RIBBON(0x1F380),

    // U+1F381	wrapped present
    WRAPPED_PRESENT(0x1F381),

    // U+1F382	birthday cake
    BIRTHDAY_CAKE(0x1F382),

    // U+1F383	jack-o-lantern
    JACK_O_LANTERN(0x1F383),

    // U+1F384	christmas tree
    CHRISTMAS_TREE(0x1F384),

    // U+1F385	father christmas
    FATHER_CHRISTMAS(0x1F385),

    // U+1F386	fireworks
    FIREWORKS(0x1F386),

    // U+1F387	firework sparkler
    FIREWORK_SPARKLER(0x1F387),

    // U+1F388	balloon
    BALLOON(0x1F388),

    // U+1F389	party popper
    PARTY_POPPER(0x1F389),

    // U+1F38A	confetti ball
    CONFETTI_BALL(0x1F38A),

    // U+1F38B	tanabata tree
    TANABATA_TREE(0x1F38B),

    // U+1F38C	crossed flags
    CROSSED_FLAGS(0x1F38C),

    // U+1F38D	pine decoration
    PINE_DECORATION(0x1F38D),

    // U+1F38E	japanese dolls
    JAPANESE_DOLLS(0x1F38E),

    // U+1F38F	carp streamer
    CARP_STREAMER(0x1F38F),

    // U+1F390	wind chime
    WIND_CHIME(0x1F390),

    // U+1F391	moon viewing ceremony
    MOON_VIEWING_CEREMONY(0x1F391),

    // U+1F392	school satchel
    SCHOOL_SATCHEL(0x1F392),

    // U+1F393	graduation cap
    GRADUATION_CAP(0x1F393),

    // U+1F3A0	carousel horse
    CAROUSEL_HORSE(0x1F3A0),

    // U+1F3A1	ferris wheel
    FERRIS_WHEEL(0x1F3A1),

    // U+1F3A2	roller coaster
    ROLLER_COASTER(0x1F3A2),

    // U+1F3A3	fishing pole and fish
    FISHING_POLE_AND_FISH(0x1F3A3),

    // U+1F3A4	microphone
    MICROPHONE(0x1F3A4),

    // U+1F3A5	movie camera
    MOVIE_CAMERA(0x1F3A5),

    // U+1F3A6	cinema
    CINEMA(0x1F3A6),

    // U+1F3A7	headphone
    HEADPHONE(0x1F3A7),

    // U+1F3A8	artist palette
    ARTIST_PALETTE(0x1F3A8),

    // U+1F3A9	top hat
    TOP_HAT(0x1F3A9),

    // U+1F3AA	circus tent
    CIRCUS_TENT(0x1F3AA),

    // U+1F3AB	ticket
    TICKET(0x1F3AB),

    // U+1F3AC	clapper board
    CLAPPER_BOARD(0x1F3AC),

    // U+1F3AD	performing arts
    PERFORMING_ARTS(0x1F3AD),

    // U+1F3AE	video game
    VIDEO_GAME(0x1F3AE),

    // U+1F3AF	direct hit
    DIRECT_HIT(0x1F3AF),

    // U+1F3B0	slot machine
    SLOT_MACHINE(0x1F3B0),

    // U+1F3B1	billiards
    BILLIARDS(0x1F3B1),

    // U+1F3B2	game die
    GAME_DIE(0x1F3B2),

    // U+1F3B3	bowling
    BOWLING(0x1F3B3),

    // U+1F3B4	flower playing cards
    FLOWER_PLAYING_CARDS(0x1F3B4),

    // U+1F3B5	musical note
    MUSICAL_NOTE(0x1F3B5),

    // U+1F3B6	multiple musical notes
    MULTIPLE_MUSICAL_NOTES(0x1F3B6),

    // U+1F3B7	saxophone
    SAXOPHONE(0x1F3B7),

    // U+1F3B8	guitar
    GUITAR(0x1F3B8),

    // U+1F3B9	musical keyboard
    MUSICAL_KEYBOARD(0x1F3B9),

    // U+1F3BA	trumpet
    TRUMPET(0x1F3BA),

    // U+1F3BB	violin
    VIOLIN(0x1F3BB),

    // U+1F3BC	musical score
    MUSICAL_SCORE(0x1F3BC),

    // U+1F3BD	running shirt with sash
    RUNNING_SHIRT_WITH_SASH(0x1F3BD),

    // U+1F3BE	tennis racquet and ball
    TENNIS_RACQUET_AND_BALL(0x1F3BE),

    // U+1F3BF	ski and ski boot
    SKI_AND_SKI_BOOT(0x1F3BF),

    // U+1F3C0	basketball and hoop
    BASKETBALL_AND_HOOP(0x1F3C0),

    // U+1F3C1	chequered flag
    CHEQUERED_FLAG(0x1F3C1),

    // U+1F3C2	snowboarder
    SNOWBOARDER(0x1F3C2),

    // U+1F3C3	runner
    RUNNER(0x1F3C3),

    // U+1F3C4	surfer
    SURFER(0x1F3C4),

    // U+1F3C6	trophy
    TROPHY(0x1F3C6),

    // U+1F3C8	american football
    AMERICAN_FOOTBALL(0x1F3C8),

    // U+1F3CA	swimmer
    SWIMMER(0x1F3CA),

    // U+1F3E0	house building
    HOUSE_BUILDING(0x1F3E0),

    // U+1F3E1	house with garden
    HOUSE_WITH_GARDEN(0x1F3E1),

    // U+1F3E2	office building
    OFFICE_BUILDING(0x1F3E2),

    // U+1F3E3	japanese post office
    JAPANESE_POST_OFFICE(0x1F3E3),

    // U+1F3E5	hospital
    HOSPITAL(0x1F3E5),

    // U+1F3E6	bank
    BANK(0x1F3E6),

    // U+1F3E7	automated teller machine
    AUTOMATED_TELLER_MACHINE(0x1F3E7),

    // U+1F3E8	hotel
    HOTEL(0x1F3E8),

    // U+1F3E9	love hotel
    LOVE_HOTEL(0x1F3E9),

    // U+1F3EA	convenience store
    CONVENIENCE_STORE(0x1F3EA),

    // U+1F3EB	school
    SCHOOL(0x1F3EB),

    // U+1F3EC	department store
    DEPARTMENT_STORE(0x1F3EC),

    // U+1F3ED	factory
    FACTORY(0x1F3ED),

    // U+1F3EE	izakaya lantern
    IZAKAYA_LANTERN(0x1F3EE),

    // U+1F3EF	japanese castle
    JAPANESE_CASTLE(0x1F3EF),

    // U+1F3F0	european castle
    EUROPEAN_CASTLE(0x1F3F0),

    // U+1F40C	snail
    SNAIL(0x1F40C),

    // U+1F40D	snake
    SNAKE(0x1F40D),

    // U+1F40E	horse
    HORSE(0x1F40E),

    // U+1F411	sheep
    SHEEP(0x1F411),

    // U+1F412	monkey
    MONKEY(0x1F412),

    // U+1F414	chicken
    CHICKEN(0x1F414),

    // U+1F417	boar
    BOAR(0x1F417),

    // U+1F418	elephant
    ELEPHANT(0x1F418),

    // U+1F419	octopus
    OCTOPUS(0x1F419),

    // U+1F41A	spiral shell
    SPIRAL_SHELL(0x1F41A),

    // U+1F41B	bug
    BUG(0x1F41B),

    // U+1F41C	ant
    ANT(0x1F41C),

    // U+1F41D	honeybee
    HONEYBEE(0x1F41D),

    // U+1F41E	lady beetle
    LADY_BEETLE(0x1F41E),

    // U+1F41F	fish
    FISH(0x1F41F),

    // U+1F420	tropical fish
    TROPICAL_FISH(0x1F420),

    // U+1F421	blowfish
    BLOWFISH(0x1F421),

    // U+1F422	turtle
    TURTLE(0x1F422),

    // U+1F423	hatching chick
    HATCHING_CHICK(0x1F423),

    // U+1F424	baby chick
    BABY_CHICK(0x1F424),

    // U+1F425	front-facing baby chick
    FRONT_FACING_BABY_CHICK(0x1F425),

    // U+1F426	bird
    BIRD(0x1F426),

    // U+1F427	penguin
    PENGUIN(0x1F427),

    // U+1F428	koala
    KOALA(0x1F428),

    // U+1F429	poodle
    POODLE(0x1F429),

    // U+1F42B	bactrian camel
    BACTRIAN_CAMEL(0x1F42B),

    // U+1F42C	dolphin
    DOLPHIN(0x1F42C),

    // U+1F42D	mouse face
    MOUSE_FACE(0x1F42D),

    // U+1F42E	cow face
    COW_FACE(0x1F42E),

    // U+1F42F	tiger face
    TIGER_FACE(0x1F42F),

    // U+1F430	rabbit face
    RABBIT_FACE(0x1F430),

    // U+1F431	cat face
    CAT_FACE(0x1F431),

    // U+1F432	dragon face
    DRAGON_FACE(0x1F432),

    // U+1F433	spouting whale
    SPOUTING_WHALE(0x1F433),

    // U+1F434	horse face
    HORSE_FACE(0x1F434),

    // U+1F435	monkey face
    MONKEY_FACE(0x1F435),

    // U+1F436	dog face
    DOG_FACE(0x1F436),

    // U+1F437	pig face
    PIG_FACE(0x1F437),

    // U+1F438	frog face
    FROG_FACE(0x1F438),

    // U+1F439	hamster face
    HAMSTER_FACE(0x1F439),

    // U+1F43A	wolf face
    WOLF_FACE(0x1F43A),

    // U+1F43B	bear face
    BEAR_FACE(0x1F43B),

    // U+1F43C	panda face
    PANDA_FACE(0x1F43C),

    // U+1F43D	pig nose
    PIG_NOSE(0x1F43D),

    // U+1F43E	paw prints
    PAW_PRINTS(0x1F43E),

    // U+1F440	eyes
    EYES(0x1F440),

    // U+1F442	ear
    EAR(0x1F442),

    // U+1F443	nose
    NOSE(0x1F443),

    // U+1F444	mouth
    MOUTH(0x1F444),

    // U+1F445	tongue
    TONGUE(0x1F445),

    // U+1F446	white up pointing backhand index
    WHITE_UP_POINTING_BACKHAND_INDEX(0x1F446),

    // U+1F447	white down pointing backhand index
    WHITE_DOWN_POINTING_BACKHAND_INDEX(0x1F447),

    // U+1F448	white left pointing backhand index
    WHITE_LEFT_POINTING_BACKHAND_INDEX(0x1F448),

    // U+1F449	white right pointing backhand index
    WHITE_RIGHT_POINTING_BACKHAND_INDEX(0x1F449),

    // U+1F44A	fisted hand sign
    FISTED_HAND_SIGN(0x1F44A),

    // U+1F44B	waving hand sign
    WAVING_HAND_SIGN(0x1F44B),

    // U+1F44C	ok hand sign
    OK_HAND_SIGN(0x1F44C),

    // U+1F44D	thumbs up sign
    THUMBS_UP_SIGN(0x1F44D),

    // U+1F44E	thumbs down sign
    THUMBS_DOWN_SIGN(0x1F44E),

    // U+1F44F	clapping hands sign
    CLAPPING_HANDS_SIGN(0x1F44F),

    // U+1F450	open hands sign
    OPEN_HANDS_SIGN(0x1F450),

    // U+1F451	crown
    CROWN(0x1F451),

    // U+1F452	womans hat
    WOMANS_HAT(0x1F452),

    // U+1F453	eyeglasses
    EYEGLASSES(0x1F453),

    // U+1F454	necktie
    NECKTIE(0x1F454),

    // U+1F455	t-shirt
    T_SHIRT(0x1F455),

    // U+1F456	jeans
    JEANS(0x1F456),

    // U+1F457	dress
    DRESS(0x1F457),

    // U+1F458	kimono
    KIMONO(0x1F458),

    // U+1F459	bikini
    BIKINI(0x1F459),

    // U+1F45A	womans clothes
    WOMANS_CLOTHES(0x1F45A),

    // U+1F45B	purse
    PURSE(0x1F45B),

    // U+1F45C	handbag
    HANDBAG(0x1F45C),

    // U+1F45D	pouch
    POUCH(0x1F45D),

    // U+1F45E	mans shoe
    MANS_SHOE(0x1F45E),

    // U+1F45F	athletic shoe
    ATHLETIC_SHOE(0x1F45F),

    // U+1F460	high-heeled shoe
    HIGH_HEELED_SHOE(0x1F460),

    // U+1F461	womans sandal
    WOMANS_SANDAL(0x1F461),

    // U+1F462	womans boots
    WOMANS_BOOTS(0x1F462),

    // U+1F463	footprints
    FOOTPRINTS(0x1F463),

    // U+1F464	bust in silhouette
    BUST_IN_SILHOUETTE(0x1F464),

    // U+1F466	boy
    BOY(0x1F466),

    // U+1F467	girl
    GIRL(0x1F467),

    // U+1F468	man
    MAN(0x1F468),

    // U+1F469	woman
    WOMAN(0x1F469),

    // U+1F46A	family
    FAMILY(0x1F46A),

    // U+1F46B	man and woman holding hands
    MAN_AND_WOMAN_HOLDING_HANDS(0x1F46B),

    // U+1F46E	police officer
    POLICE_OFFICER(0x1F46E),

    // U+1F46F	woman with bunny ears
    WOMAN_WITH_BUNNY_EARS(0x1F46F),

    // U+1F470	bride with veil
    BRIDE_WITH_VEIL(0x1F470),

    // U+1F471	person with blond hair
    PERSON_WITH_BLOND_HAIR(0x1F471),

    // U+1F472	man with gua pi mao
    MAN_WITH_GUA_PI_MAO(0x1F472),

    // U+1F473	man with turban
    MAN_WITH_TURBAN(0x1F473),

    // U+1F474	older man
    OLDER_MAN(0x1F474),

    // U+1F475	older woman
    OLDER_WOMAN(0x1F475),

    // U+1F476	baby
    BABY(0x1F476),

    // U+1F477	construction worker
    CONSTRUCTION_WORKER(0x1F477),

    // U+1F478	princess
    PRINCESS(0x1F478),

    // U+1F479	japanese ogre
    JAPANESE_OGRE(0x1F479),

    // U+1F47A	japanese goblin
    JAPANESE_GOBLIN(0x1F47A),

    // U+1F47B	ghost
    GHOST(0x1F47B),

    // U+1F47C	baby angel
    BABY_ANGEL(0x1F47C),

    // U+1F47D	extraterrestrial alien
    EXTRATERRESTRIAL_ALIEN(0x1F47D),

    // U+1F47E	alien monster
    ALIEN_MONSTER(0x1F47E),

    // U+1F47F	imp
    IMP(0x1F47F),

    // U+1F480	skull
    SKULL(0x1F480),

    // U+1F481	information desk person
    INFORMATION_DESK_PERSON(0x1F481),

    // U+1F482	guardsman
    GUARDSMAN(0x1F482),

    // U+1F483	dancer
    DANCER(0x1F483),

    // U+1F484	lipstick
    LIPSTICK(0x1F484),

    // U+1F485	nail polish
    NAIL_POLISH(0x1F485),

    // U+1F486	face massage
    FACE_MASSAGE(0x1F486),

    // U+1F487	haircut
    HAIRCUT(0x1F487),

    // U+1F488	barber pole
    BARBER_POLE(0x1F488),

    // U+1F489	syringe
    SYRINGE(0x1F489),

    // U+1F48A	pill
    PILL(0x1F48A),

    // U+1F48B	kiss mark
    KISS_MARK(0x1F48B),

    // U+1F48C	love letter
    LOVE_LETTER(0x1F48C),

    // U+1F48D	ring
    RING(0x1F48D),

    // U+1F48E	gem stone
    GEM_STONE(0x1F48E),

    // U+1F48F	kiss
    KISS(0x1F48F),

    // U+1F490	bouquet
    BOUQUET(0x1F490),

    // U+1F491	couple with heart
    COUPLE_WITH_HEART(0x1F491),

    // U+1F492	wedding
    WEDDING(0x1F492),

    // U+1F493	beating heart
    BEATING_HEART(0x1F493),

    // U+1F494	broken heart
    BROKEN_HEART(0x1F494),

    // U+1F495	two hearts
    TWO_HEARTS(0x1F495),

    // U+1F496	sparkling heart
    SPARKLING_HEART(0x1F496),

    // U+1F497	growing heart
    GROWING_HEART(0x1F497),

    // U+1F498	heart with arrow
    HEART_WITH_ARROW(0x1F498),

    // U+1F499	blue heart
    BLUE_HEART(0x1F499),

    // U+1F49A	green heart
    GREEN_HEART(0x1F49A),

    // U+1F49B	yellow heart
    YELLOW_HEART(0x1F49B),

    // U+1F49C	purple heart
    PURPLE_HEART(0x1F49C),

    // U+1F49D	heart with ribbon
    HEART_WITH_RIBBON(0x1F49D),

    // U+1F49E	revolving hearts
    REVOLVING_HEARTS(0x1F49E),

    // U+1F49F	heart decoration
    HEART_DECORATION(0x1F49F),

    // U+1F4A0	diamond shape with a dot inside
    DIAMOND_SHAPE_WITH_A_DOT_INSIDE(0x1F4A0),

    // U+1F4A1	electric light bulb
    ELECTRIC_LIGHT_BULB(0x1F4A1),

    // U+1F4A2	anger symbol
    ANGER_SYMBOL(0x1F4A2),

    // U+1F4A3	bomb
    BOMB(0x1F4A3),

    // U+1F4A4	sleeping symbol
    SLEEPING_SYMBOL(0x1F4A4),

    // U+1F4A5	collision symbol
    COLLISION_SYMBOL(0x1F4A5),

    // U+1F4A6	splashing sweat symbol
    SPLASHING_SWEAT_SYMBOL(0x1F4A6),

    // U+1F4A7	droplet
    DROPLET(0x1F4A7),

    // U+1F4A8	dash symbol
    DASH_SYMBOL(0x1F4A8),

    // U+1F4A9	pile of poo
    PILE_OF_POO(0x1F4A9),

    // U+1F4AA	flexed biceps
    FLEXED_BICEPS(0x1F4AA),

    // U+1F4AB	dizzy symbol
    DIZZY_SYMBOL(0x1F4AB),

    // U+1F4AC	speech balloon
    SPEECH_BALLOON(0x1F4AC),

    // U+1F4AE	white flower
    WHITE_FLOWER(0x1F4AE),

    // U+1F4AF	hundred points symbol
    HUNDRED_POINTS_SYMBOL(0x1F4AF),

    // U+1F4B0	money bag
    MONEY_BAG(0x1F4B0),

    // U+1F4B1	currency exchange
    CURRENCY_EXCHANGE(0x1F4B1),

    // U+1F4B2	heavy dollar sign
    HEAVY_DOLLAR_SIGN(0x1F4B2),

    // U+1F4B3	credit card
    CREDIT_CARD(0x1F4B3),

    // U+1F4B4	banknote with yen sign
    BANKNOTE_WITH_YEN_SIGN(0x1F4B4),

    // U+1F4B5	banknote with dollar sign
    BANKNOTE_WITH_DOLLAR_SIGN(0x1F4B5),

    // U+1F4B8	money with wings
    MONEY_WITH_WINGS(0x1F4B8),

    // U+1F4B9	chart with upwards trend and yen sign
    CHART_WITH_UPWARDS_TREND_AND_YEN_SIGN(0x1F4B9),

    // U+1F4BA	seat
    SEAT(0x1F4BA),

    // U+1F4BB	personal computer
    PERSONAL_COMPUTER(0x1F4BB),

    // U+1F4BC	briefcase
    BRIEFCASE(0x1F4BC),

    // U+1F4BD	minidisc
    MINIDISC(0x1F4BD),

    // U+1F4BE	floppy disk
    FLOPPY_DISK(0x1F4BE),

    // U+1F4BF	optical disc
    OPTICAL_DISC(0x1F4BF),

    // U+1F4C0	dvd
    DVD(0x1F4C0),

    // U+1F4C1	file folder
    FILE_FOLDER(0x1F4C1),

    // U+1F4C2	open file folder
    OPEN_FILE_FOLDER(0x1F4C2),

    // U+1F4C3	page with curl
    PAGE_WITH_CURL(0x1F4C3),

    // U+1F4C4	page facing up
    PAGE_FACING_UP(0x1F4C4),

    // U+1F4C5	calendar
    CALENDAR(0x1F4C5),

    // U+1F4C6	tear-off calendar
    TEAR_OFF_CALENDAR(0x1F4C6),

    // U+1F4C7	card index
    CARD_INDEX(0x1F4C7),

    // U+1F4C8	chart with upwards trend
    CHART_WITH_UPWARDS_TREND(0x1F4C8),

    // U+1F4C9	chart with downwards trend
    CHART_WITH_DOWNWARDS_TREND(0x1F4C9),

    // U+1F4CA	bar chart
    BAR_CHART(0x1F4CA),

    // U+1F4CB	clipboard
    CLIPBOARD(0x1F4CB),

    // U+1F4CC	pushpin
    PUSHPIN(0x1F4CC),

    // U+1F4CD	round pushpin
    ROUND_PUSHPIN(0x1F4CD),

    // U+1F4CE	paperclip
    PAPERCLIP(0x1F4CE),

    // U+1F4CF	straight ruler
    STRAIGHT_RULER(0x1F4CF),

    // U+1F4D0	triangular ruler
    TRIANGULAR_RULER(0x1F4D0),

    // U+1F4D1	bookmark tabs
    BOOKMARK_TABS(0x1F4D1),

    // U+1F4D2	ledger
    LEDGER(0x1F4D2),

    // U+1F4D3	notebook
    NOTEBOOK(0x1F4D3),

    // U+1F4D4	notebook with decorative cover
    NOTEBOOK_WITH_DECORATIVE_COVER(0x1F4D4),

    // U+1F4D5	closed book
    CLOSED_BOOK(0x1F4D5),

    // U+1F4D6	open book
    OPEN_BOOK(0x1F4D6),

    // U+1F4D7	green book
    GREEN_BOOK(0x1F4D7),

    // U+1F4D8	blue book
    BLUE_BOOK(0x1F4D8),

    // U+1F4D9	orange book
    ORANGE_BOOK(0x1F4D9),

    // U+1F4DA	books
    BOOKS(0x1F4DA),

    // U+1F4DB	name badge
    NAME_BADGE(0x1F4DB),

    // U+1F4DC	scroll
    SCROLL(0x1F4DC),

    // U+1F4DD	memo
    MEMO(0x1F4DD),

    // U+1F4DE	telephone receiver
    TELEPHONE_RECEIVER(0x1F4DE),

    // U+1F4DF	pager
    PAGER(0x1F4DF),

    // U+1F4E0	fax machine
    FAX_MACHINE(0x1F4E0),

    // U+1F4E1	satellite antenna
    SATELLITE_ANTENNA(0x1F4E1),

    // U+1F4E2	public address loudspeaker
    PUBLIC_ADDRESS_LOUDSPEAKER(0x1F4E2),

    // U+1F4E3	cheering megaphone
    CHEERING_MEGAPHONE(0x1F4E3),

    // U+1F4E4	outbox tray
    OUTBOX_TRAY(0x1F4E4),

    // U+1F4E5	inbox tray
    INBOX_TRAY(0x1F4E5),

    // U+1F4E6	package
    PACKAGE(0x1F4E6),

    // U+1F4E7	e-mail symbol
    E_MAIL_SYMBOL(0x1F4E7),

    // U+1F4E8	incoming envelope
    INCOMING_ENVELOPE(0x1F4E8),

    // U+1F4E9	envelope with downwards arrow above
    ENVELOPE_WITH_DOWNWARDS_ARROW_ABOVE(0x1F4E9),

    // U+1F4EA	closed mailbox with lowered flag
    CLOSED_MAILBOX_WITH_LOWERED_FLAG(0x1F4EA),

    // U+1F4EB	closed mailbox with raised flag
    CLOSED_MAILBOX_WITH_RAISED_FLAG(0x1F4EB),

    // U+1F4EE	postbox
    POSTBOX(0x1F4EE),

    // U+1F4F0	newspaper
    NEWSPAPER(0x1F4F0),

    // U+1F4F1	mobile phone
    MOBILE_PHONE(0x1F4F1),

    // U+1F4F2	mobile phone with rightwards arrow at left
    MOBILE_PHONE_WITH_RIGHTWARDS_ARROW_AT_LEFT(0x1F4F2),

    // U+1F4F3	vibration mode
    VIBRATION_MODE(0x1F4F3),

    // U+1F4F4	mobile phone off
    MOBILE_PHONE_OFF(0x1F4F4),

    // U+1F4F6	antenna with bars
    ANTENNA_WITH_BARS(0x1F4F6),

    // U+1F4F7	camera
    CAMERA(0x1F4F7),

    // U+1F4F9	video camera
    VIDEO_CAMERA(0x1F4F9),

    // U+1F4FA	television
    TELEVISION(0x1F4FA),

    // U+1F4FB	radio
    RADIO(0x1F4FB),

    // U+1F4FC	videocassette
    VIDEOCASSETTE(0x1F4FC),

    // U+1F503	clockwise downwards and upwards open circle arrows
    CLOCKWISE_DOWNWARDS_AND_UPWARDS_OPEN_CIRCLE_ARROWS(0x1F503),

    // U+1F50A	speaker with three sound waves
    SPEAKER_WITH_THREE_SOUND_WAVES(0x1F50A),

    // U+1F50B	battery
    BATTERY(0x1F50B),

    // U+1F50C	electric plug
    ELECTRIC_PLUG(0x1F50C),

    // U+1F50D	left-pointing magnifying glass
    LEFT_POINTING_MAGNIFYING_GLASS(0x1F50D),

    // U+1F50E	right-pointing magnifying glass
    RIGHT_POINTING_MAGNIFYING_GLASS(0x1F50E),

    // U+1F50F	lock with ink pen
    LOCK_WITH_INK_PEN(0x1F50F),

    // U+1F510	closed lock with key
    CLOSED_LOCK_WITH_KEY(0x1F510),

    // U+1F511	key
    KEY(0x1F511),

    // U+1F512	lock
    LOCK(0x1F512),

    // U+1F513	open lock
    OPEN_LOCK(0x1F513),

    // U+1F514	bell
    BELL(0x1F514),

    // U+1F516	bookmark
    BOOKMARK(0x1F516),

    // U+1F517	link symbol
    LINK_SYMBOL(0x1F517),

    // U+1F518	radio button
    RADIO_BUTTON(0x1F518),

    // U+1F519	back with leftwards arrow above
    BACK_WITH_LEFTWARDS_ARROW_ABOVE(0x1F519),

    // U+1F51A	end with leftwards arrow above
    END_WITH_LEFTWARDS_ARROW_ABOVE(0x1F51A),

    // U+1F51B	on with exclamation mark with left right arrow above
    ON_WITH_EXCLAMATION_MARK_WITH_LEFT_RIGHT_ARROW_ABOVE(0x1F51B),

    // U+1F51C	soon with rightwards arrow above
    SOON_WITH_RIGHTWARDS_ARROW_ABOVE(0x1F51C),

    // U+1F51D	top with upwards arrow above
    TOP_WITH_UPWARDS_ARROW_ABOVE(0x1F51D),

    // U+1F51E	no one under eighteen symbol
    NO_ONE_UNDER_EIGHTEEN_SYMBOL(0x1F51E),

    // U+1F51F	keycap ten
    KEYCAP_TEN(0x1F51F),

    // U+1F520	input symbol for latin capital letters
    INPUT_SYMBOL_FOR_LATIN_CAPITAL_LETTERS(0x1F520),

    // U+1F521	input symbol for latin small letters
    INPUT_SYMBOL_FOR_LATIN_SMALL_LETTERS(0x1F521),

    // U+1F522	input symbol for numbers
    INPUT_SYMBOL_FOR_NUMBERS(0x1F522),

    // U+1F523	input symbol for symbols
    INPUT_SYMBOL_FOR_SYMBOLS(0x1F523),

    // U+1F524	input symbol for latin letters
    INPUT_SYMBOL_FOR_LATIN_LETTERS(0x1F524),

    // U+1F525	fire
    FIRE(0x1F525),

    // U+1F526	electric torch
    ELECTRIC_TORCH(0x1F526),

    // U+1F527	wrench
    WRENCH(0x1F527),

    // U+1F528	hammer
    HAMMER(0x1F528),

    // U+1F529	nut and bolt
    NUT_AND_BOLT(0x1F529),

    // U+1F52A	hocho
    HOCHO(0x1F52A),

    // U+1F52B	pistol
    PISTOL(0x1F52B),

    // U+1F52E	crystal ball
    CRYSTAL_BALL(0x1F52E),

    // U+1F52F	six pointed star with middle dot
    SIX_POINTED_STAR_WITH_MIDDLE_DOT(0x1F52F),

    // U+1F530	japanese symbol for beginner
    JAPANESE_SYMBOL_FOR_BEGINNER(0x1F530),

    // U+1F531	trident emblem
    TRIDENT_EMBLEM(0x1F531),

    // U+1F532	black square button
    BLACK_SQUARE_BUTTON(0x1F532),

    // U+1F533	white square button
    WHITE_SQUARE_BUTTON(0x1F533),

    // U+1F534	large red circle
    LARGE_RED_CIRCLE(0x1F534),

    // U+1F535	large blue circle
    LARGE_BLUE_CIRCLE(0x1F535),

    // U+1F536	large orange diamond
    LARGE_ORANGE_DIAMOND(0x1F536),

    // U+1F537	large blue diamond
    LARGE_BLUE_DIAMOND(0x1F537),

    // U+1F538	small orange diamond
    SMALL_ORANGE_DIAMOND(0x1F538),

    // U+1F539	small blue diamond
    SMALL_BLUE_DIAMOND(0x1F539),

    // U+1F53A	up-pointing red triangle
    UP_POINTING_RED_TRIANGLE(0x1F53A),

    // U+1F53B	down-pointing red triangle
    DOWN_POINTING_RED_TRIANGLE(0x1F53B),

    // U+1F53C	up-pointing small red triangle
    UP_POINTING_SMALL_RED_TRIANGLE(0x1F53C),

    // U+1F53D	down-pointing small red triangle
    DOWN_POINTING_SMALL_RED_TRIANGLE(0x1F53D),

    // U+1F550	clock face one oclock
    CLOCK_FACE_ONE_OCLOCK(0x1F550),

    // U+1F551	clock face two oclock
    CLOCK_FACE_TWO_OCLOCK(0x1F551),

    // U+1F552	clock face three oclock
    CLOCK_FACE_THREE_OCLOCK(0x1F552),

    // U+1F553	clock face four oclock
    CLOCK_FACE_FOUR_OCLOCK(0x1F553),

    // U+1F554	clock face five oclock
    CLOCK_FACE_FIVE_OCLOCK(0x1F554),

    // U+1F555	clock face six oclock
    CLOCK_FACE_SIX_OCLOCK(0x1F555),

    // U+1F556	clock face seven oclock
    CLOCK_FACE_SEVEN_OCLOCK(0x1F556),

    // U+1F557	clock face eight oclock
    CLOCK_FACE_EIGHT_OCLOCK(0x1F557),

    // U+1F558	clock face nine oclock
    CLOCK_FACE_NINE_OCLOCK(0x1F558),

    // U+1F559	clock face ten oclock
    CLOCK_FACE_TEN_OCLOCK(0x1F559),

    // U+1F55A	clock face eleven oclock
    CLOCK_FACE_ELEVEN_OCLOCK(0x1F55A),

    // U+1F55B	clock face twelve oclock
    CLOCK_FACE_TWELVE_OCLOCK(0x1F55B),

    // U+1F5FB	mount fuji
    MOUNT_FUJI(0x1F5FB),

    // U+1F5FC	tokyo tower
    TOKYO_TOWER(0x1F5FC),

    // U+1F5FD	statue of liberty
    STATUE_OF_LIBERTY(0x1F5FD),

    // U+1F5FE	silhouette of japan
    SILHOUETTE_OF_JAPAN(0x1F5FE),

    // U+1F5FF	moyai
    MOYAI(0x1F5FF),

    // U+1F600	grinning face
    GRINNING_FACE(0x1F600),

    // U+1F607	smiling face with halo
    SMILING_FACE_WITH_HALO(0x1F607),

    // U+1F608	smiling face with horns
    SMILING_FACE_WITH_HORNS(0x1F608),

    // U+1F60E	smiling face with sunglasses
    SMILING_FACE_WITH_SUNGLASSES(0x1F60E),

    // U+1F610	neutral face
    NEUTRAL_FACE(0x1F610);

    int code;

    Emoji(int unicode) {
        this.code = unicode;
    }

    @Override
    public String toString() {
        return new String(Character.toChars(code));
    }
}
