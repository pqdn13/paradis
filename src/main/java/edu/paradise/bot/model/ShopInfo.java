package edu.paradise.bot.model;

public class ShopInfo {
    private String name;
    private String description;
    private String contact;

    public ShopInfo(String name, String description, String contact) {
        this.name = name;
        this.description = description;
        this.contact = contact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
