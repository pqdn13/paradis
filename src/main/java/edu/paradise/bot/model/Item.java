package edu.paradise.bot.model;

public class Item {
    private String name;
    private String description;
    private long id;
    private long price;

    public Item(String name, String description, long id, long price) {
        this.name = name;
        this.description = description;
        this.id = id;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getUrlImg(){
        return "https://dodopizzaru-a.akamaihd.net/Img/Products/Pizza/39c03797-9ad1-4ab2-b419-6ecc0e962063.jpg";
    }
}
