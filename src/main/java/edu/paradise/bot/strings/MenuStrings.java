package edu.paradise.bot.strings;

import edu.paradise.bot.emoji.Emoji;

@Deprecated
public class MenuStrings {
    public String getChooseProduct() {
        return Emoji.SLICE_OF_PIZZA.toString() + "Выбрать пиццу";
    }

    public String getInfo() {
        return Emoji.INFORMATION_SOURCE.toString() + "Инфо";
    }

    public String getInitMenuMessage() {
        return "Бот инициализирован, удачных покупок!" + Emoji.CAT_FACE.toString();
    }

    public String getInfoMessage() {
        return "Пиццерия <b>\"Вкусная сардиния\"</b>\nЖелаем приятного времяпровождения с нашей пицей\nНаши контакты: ******\n" + getEvent();
    }

    public String getEvent() {
        return "Сегодня скидки на пиццу моцарелла";
    }
}
